# 1.
for i in range(151):
    print(i):

# 2.
for i in range(0,1000,5):
    print(i):

# 3.
for i in range(1,100,1):
    if i % 5 == 0:
        print("Coding")
    if i % 10 == 0:
        print("Dojo")

# 4.
var count = 0
for i in range(1,500000,2):
    count += i:
        print(i):
        print(count):

# 5.
for i in range(2018,0,-4):
    print(i):

# 6.
lowNum = 2
highNum = 9
mult = 3