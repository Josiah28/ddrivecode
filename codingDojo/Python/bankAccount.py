class bankAccount:
    def __init__(self):
        self.intRate = 0.01
        self.balance = 0

    def deposit(self, amount):
        self.balance += amount
        return self
    def withdrawal(self, amount):
        self.balance -= amount
        return self

    def accountInfo(self):
        print(self.balance)
        return self

    def yieldInt(self):
        print(self.intRate)
        return self

    def check_balance(self):
        print(self.balance)
        return self
        
kenna = bankAccount()
josiah = bankAccount()

kenna.deposit(420).deposit(1500).deposit(2500).withdrawal(320).yieldInt().check_balance().accountInfo()
josiah.deposit(320).deposit(1000).deposit(2250).withdrawal(420).yieldInt().check_balance().accountInfo()