class User:
    def __init__(self, name):
        self.name = name
        intRate = .02
        balance = 0
        self.account = BankAccount(intRate, balance)

    def deposit(self, amount):
        self.account.deposit(amount)
        return self

    def withdrawal(self, amount):
        self.account.withdrawal(amount)
        return self

    def check_balance(self):
        print(self.account)



class BankAccount:
    def __init__(self, interest = 0.01, moneyBalance = 10):
        self.accountInterest = interest
        self.accountBalance = moneyBalance

    def deposit(self, amount):
        self.accountBalance += amount
        return self
    def withdrawal(self, amount):
        self.accountBalance -= amount
        return self

    def accountInfo(self):
        print(self.accountBalance)
        return self

    def yieldInt(self):
        print(self.accountInterest)
        return self

    def check_balance(self):
        print(self.accountBalance)
        return self
        
josiah = User('Josiah')
kenna = User('Kenna')
dex = User('Dex')

kenna = BankAccount()
josiah = BankAccount()

kenna.deposit(2500).withdrawal(500).check_balance()
josiah.deposit(1000).deposit(1500).withdrawal(500).check_balance()