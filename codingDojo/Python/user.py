class User:
    def __init__(self, name):
        self.name = name,
        self.balance = 0

    def deposit(self, amount):
        self.balance += amount
        return self

    def withdrawal(self, amount):
        self.balance -= amount
        return self

    def check_balance(self):
        print(self.balance)

josiah = User('Josiah')
kenna = User('Kenna')
dex = User('Dex')

josiah.deposit(1000).deposit(1000).deposit(1000).withdrawal(250).check_balance()

kenna.deposit(10000).deposit(20000).withdrawal(3000).withdrawal(1500).check_balance()

dex.deposit(500).withdrawal(69).withdrawal(20).withdrawal(45).check_balance()