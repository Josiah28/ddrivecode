from flask import Flask, render_template, request
app = Flask(__name__)   

@app.route('/')          
def board_games():
	return 'Welcome to the board game center!'

@app.route('/monopoly')
def monopoly():
	return 'Ruining families and friend circles since 1903!'

@app.route('/form', methods = ['GET']) #methods = ['GET'] (and only 'GET') is optional
def formGet():
	return render_template("form.html")

@app.route('/request', methods = ['POST'])
def showRequest():
	print(request.form)
	return render_template("request.html")

if __name__ == "__main__":   
	app.run(debug=True)   