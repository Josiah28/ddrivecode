// 1.
function pos(arr){
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            arr[i] = 'big';
        }
    }
    console.log(arr);
}
pos([-1,4,8,3,-5]);

// 2.
function lowHigh(arr){
    var min = arr[0];
    var max = arr[0];
    for(var i = 0; i < arr.length; i++){
        if(min > arr[i]){
            min = arr[i];
        }
        if(max < arr[i]){
            max = arr[i];
        }
    }
    console.log(min);
    return max;
}
lowHigh([1,2,3,4,5]);

// 3.
function printReturn(arr){
    var max = arr[0];
    for(var i = 0; i < arr.length; i++){
        if(max < arr[i]){
            max = arr[i];
        }
    }
    console.log(arr[arr.length-2]);
    return max;

}
printReturn([1,2,3,14,9]);

// 4. come back to
function double(arr){
    arr2 = [];
    for(var i = 0; i < arr.length; i++){
        arr[i] = arr[i]*2;
        arr2.push(arr[i]);
    }
    console.log(arr2);
    return arr2;
}
console.log(double([2,4,6]));

// 5.
var arr = [1,2,3,-4,5];
var count = 0;
function pos(){
    for(var i = 0; i < arr.length; i++){
        if(arr[i] > 0){
            count += 1;
            arr[arr.length-1] = count;
        }
    }
    console.log(arr);
}
pos();

//6.
function evenOdd(arr){
    var count = 0;
    var count2 = 0;
    for(var i = 0; i < arr.length; i++){
        if(arr[i] % 2 == 0){
            count += 1;
            count2 = 0;
            console.log("I found an even.");
        }
        if(count == 3){
            count = 0;
            console.log("Even more so!");
        }
        
        if(arr[i] % 2 !== 0){
            count2 += 1;
            count = 0;
            console.log("I found an odd.");
        }
        if(count2 == 3){
            count2 = 0;
            console.log("That's odd!");
        }
    }
}
evenOdd([1,3,5,4,6,8]);

// 7.
function odd(arr){
    for(var i = 0; i < arr.length; i += 2){
        arr[i] += 1;
    }
    console.log(arr);
}
odd([1,2,3,4,5,6]);

// 8.
function replace(arr){
    arr[2] = arr[1].length;
    arr[1] = arr[0].length;  
    console.log(arr);
}
replace(["hello", "dojo", "Kenna"]);

// 9.
function addSeven(arr){
    var newArr = [];
    for(var i = 0; i < arr.length; i++){
        newArr.push(arr[i] + 7);
    }
    console.log(newArr);
}
addSeven([3,5,7]);

// 10.
function reverse(arr){
    var newArr = [];
    for(var i = arr.length-1; i >= 0; i--){
        newArr.push(arr[i]);
    }
    console.log(newArr);
}
reverse([1,2,3]);

// 11.
function negative(arr){
    var newArr = [];
    for(var i = 0; i < arr.length; i++){
        newArr.push(arr[i] * -1);
    }
    console.log(newArr);
}
negative([1,2,3,4,5]);

// 12.
function food(arr){
    for(var i = 0; i < arr.length; i++){
        if(arr[i] == "food"){
            console.log("yummy");
        } else{
            console.log("I'm hungry");
        }
    }
    console.log(arr);
}
food(["food", "Kenna", "gang","Josiah", "food"]);

// 13.
function swap(arr){
    var placeHolder = 0;
    var secondPlaceHolder = 0;
    for(var i = 0; i < arr.length; i++){
        placeHolder = arr[0];
        arr[0] = arr[4];
        arr[4] = placeHolder;
        secondPlaceHolder = arr[1];
        arr[1] = arr[3];
        arr[3] = secondPlaceHolder;
    }
    console.log(arr);
}
swap([1,2,3,4,5]);

// 14.
function multiply(arr,y){
    for(var i = 0; i < arr.length; i++){
         arr[i] = arr[i]*y;
    }
    console.log(arr);
}
multiply([1,2,3],3);