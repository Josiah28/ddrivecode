function bubble(arr){
    var count = 0;
    while(count < arr.length){  
      for(var x=0; x< arr.length-1; x++){
        if(arr[x]>arr[x+1]){
          var temp = arr[x];
          arr[x] = arr[x+1];
          arr[x+1] = temp;
        }
      }
      count++
    }
    return arr;
  }
   console.log(bubble([1,4,6,2,5,3,8,4,9,10]))