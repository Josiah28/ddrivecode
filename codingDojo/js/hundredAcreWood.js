var tigger = { character: "Tigger" };
tigger.north = pooh;
tigger.north.west = piglet;
tigger.north.east = bees;

var pooh = { character: "Winnie the Pooh" };
pooh.south = tigger;
pooh.east = bees;
pooh.west = piglet;
pooh.north = chris;
pooh.north.west = owl;
pooh.north.east = rabbit;

var piglet = { character: "Piglet"};
piglet.east = tigger.north;
piglet.east  = pooh;
piglet.north = owl;
piglet.north.east = chris;
piglet.east.south = tigger;

var bees = { character: "Bees" };
bees.west = pooh;
bees.west.south = tigger;
bees.north = rabbit;
bees.north.east = gopher;
bees.north.west = chris;

var owl = { character: "Owl" };
owl.south = piglet;
owl.east = chris;
owl.south.east = pooh;

var chris = { character: "Christopher Robin" };
chris.west = owl;
chris.east = rabbit;
chris.north = kanga;
chris.south = pooh;
chris.south.west = piglet;
chris.south.east = bees;

var rabbit = { character: "Rabbit" };
rabbit.south = bees;
rabbit.west = chris;
rabbit.east = gopher;
rabbit.west.north = kanga;
rabbit.south.west = pooh;

var gopher = { character: "Gopher" };
gopher.west = rabbit;
gopher.west.sout = bees;

var kanga = { character: "Kanga" };
kanga.south = chris;
kanga.north = eeyore;
kanga.north.east = heffa;
kanga.south.west = owl;
kanga.south.east = rabbit;

var eeyore = { character: "Eeyore" };
eeyore.east = heffa;
eeyore.south = kanga;

var heffa = { character: "Heffalumps" };
heffa.west = eeyore;
heffa.west.south = kanga;