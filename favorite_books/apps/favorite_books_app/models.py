from __future__ import unicode_literals
from django.db import models
import re
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

class UserManager(models.Manager):
    def basic_validator(self, postData):
        errors = {}
        if len(postData['fname']) < 2:
            errors['fname'] = "Please enter a First Name with at least 2 characters."
        if len(postData['lname']) < 2:
            errors['lname'] = "Please enter a Last Name with at least 2 characters."
        if len(postData['password']) < 8:
            errors['password'] = "Password length must be more than 8 characters."
        if (postData['password'] != postData['pcname']):
            errors['pcname']=("Password must match")
        if not EMAIL_REGEX.match(postData['email']):
            errors['email']= "Email invalid"
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()


class BookManager(models.Manager):
    def basic_validator(self, postData):
        errors = {}
        if len(postData["title"]) < 1:
            errors["title"] = "Title needed."
        if len(postData["description"])<5:
            errors["description"]="Description must be 5 characters."
        return errors



class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    uploaded_by=models.ForeignKey(User, related_name="books_uploaded")
    users_who_like = models.ManyToManyField(User, related_name="liked_books")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = BookManager()
