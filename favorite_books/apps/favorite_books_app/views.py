from django.shortcuts import render, redirect
from .models import User, Book, UserManager, BookManager
import bcrypt
from django.contrib import messages

def index(request):
    return render(request,"favorite_books_app/index.html")

def register(request):
    if request.method=="POST":
        errors = User.objects.basic_validator(request.POST)

        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashpw=bcrypt.hashpw(request.POST["password"].encode(), bcrypt.gensalt())
            
            new_user=User.objects.create(first_name=request.POST["fname"], last_name=request.POST["lname"], email=request.POST['email'], password=hashpw)
            messages.success(request, 'Successfully Registered')
            
            request.session['id']=new_user.id
            return redirect("/")

def login(request):
    if request.method=="POST":
        errors={}
        info=User.objects.filter(email=request.POST['email'])
        if len(info)==0:
            errors['email'] = "Email not in database"
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST["password"].encode(),info[0].password.encode()):
                request.session['id']=info[0].id
                return redirect("/books")
            else:
                errors['password']="wrong password"
                for key, value in errors.items():
                    messages.error(request, value)
                return redirect('/')
    return redirect("/")

def main_page(request):
    if 'id' not in request.session:
        return redirect('/')
    context={
        'user':User.objects.get(id=request.session['id']),
        'books':Book.objects.all(),
    }
    return render(request,"favorite_books_app/main_page.html", context)

def add_book(request):
    if request.method == "POST":
        errors=Book.objects.basic_validator(request.POST)
        if len(errors)>0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect("/books")
        else:
            book=Book.objects.create(title=request.POST['title'], description=request.POST["description"], uploaded_by=User.objects.get(id=request.session['id']))
            book.users_who_like.add(request.session['id'])
        return redirect('/books')
    return redirect('/books')

def add_favorites(request,id):
    if request.method == "POST":
        this_book = Book.objects.get(id=id)
        this_user = User.objects.get(id=request.session['id'])
        this_book.users_who_like.add(this_user)
        return redirect("/books")
    return redirect("/books")

def upload_info(request,id):
    context = {
        "user": User.objects.get(id=request.session['id']),
        "book": Book.objects.get(id=id)
    }
    return render(request,"favorite_books_app/user_info.html", context)

def update(request,id):
    #if book.uploaded_by in session
    if request.method=="POST":
        errors = Book.objects.basic_validator(request.POST)

        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/books/'+ str(id))
        else:
            update_book = Book.objects.get(id=id)
            update_book.title=request.POST["title"]
            update_book.description=request.POST["description"]
            update_book.save()
            return redirect('/books/'+ str(id))

def unfav(request, id):
    if request.method == "POST":
        this_book = Book.objects.get(id=id)
        this_user = User.objects.get(id=request.session['id'])
        this_book.users_who_like.remove(this_user)
        return redirect("/books")


def destroy(request, id):
    book = Book.objects.get(id=id)
    book.delete()
    return redirect("/books")



def log_out(request):
    request.session.flush()
    return redirect("/")
