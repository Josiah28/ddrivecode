from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^books$', views.main_page),
    url(r'^add_book$', views.add_book),
    url(r'^books/(?P<id>\d+)$', views.upload_info),
    url(r'^add_favorites/(?P<id>\d+)$', views.add_favorites),
    url(r'^update/(?P<id>\d+)$', views.update),
    url(r'^delete/(?P<id>\d+)$', views.destroy),
    url(r'^unfav/(?P<id>\d+)$', views.unfav),
    url(r'^out$', views.log_out)



]