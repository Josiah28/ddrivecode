from django.db import models
    
class User(models.Model):
    first_name = models.Textfield(max_length=45)
    last_name = models.TextField(max_length=45)
    email_address = models.TextField(max_length=45)
    age = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)