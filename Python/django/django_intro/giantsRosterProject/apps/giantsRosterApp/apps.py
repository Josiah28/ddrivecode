from django.apps import AppConfig


class GiantsrosterappConfig(AppConfig):
    name = 'giantsRosterApp'
