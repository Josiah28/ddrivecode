from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^home$', views.home),
    url(r'^offense$', views.offense),
    url(r'^defense$', views.defense),
    url(r'^specialTeams$', views.specialTeams),
    url(r'^logout$', views.logout)
]