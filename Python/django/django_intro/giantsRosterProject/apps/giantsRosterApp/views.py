from django.shortcuts import render, redirect
import bcrypt
from django.contrib import messages
from .models import UsersManager, Users, Players, PlayersManager

def index(request):
    return render(request, "giantsRosterApp/login.html")

def register(request):
    if request.method == "POST":
        errors = Users.objects.basic_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/') 
        else:
            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            loginId = Users.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = hashpw)
            request.session['id'] = loginId.id
            return redirect('/home')
    return redirect('/')

def login(request):
    if request.method == "POST":
        errors = {}
        info = Users.objects.filter(email = request.POST['email'])
        if len(info) == 0:
            errors["email"] = "Email not in database"
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                request.session['id'] = info[0].id
                return redirect('/home')
            if not bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                errors['password'] = "Wrong password"
                if len(errors) > 0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')
    return redirect('/')

def home(request):
    if 'id' not in request.session:
        return redirect('/')
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "players": Players.objects.all()
    }
    return render(request, "giantsRosterApp/home.html", context)

def offense(request):
    if 'id' not in request.session:
        return redirect('/')
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "players": Players.objects.all()
    }
    return render(request, "giantsRosterApp/offense.html", context)

def defense(request):
    if 'id' not in request.session:
        return redirect('/')
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "players": Players.objects.all()
    }
    return render(request, "giantsRosterApp/defense.html", context)

def specialTeams(request):
    if 'id' not in request.session:
        return redirect('/')
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "players": Players.objects.all()
    }
    return render(request, "giantsRosterApp/specialTeams.html", context)

def logout(request):
    request.session.flush()
    return redirect('/')