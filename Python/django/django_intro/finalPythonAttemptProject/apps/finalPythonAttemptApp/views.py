from django.shortcuts import render, redirect
import bcrypt
from django.contrib import messages
from .models import UsersManager, Users, Groups, GroupsManager

def index(request):
    return render(request, "finalPythonAttemptApp/index.html")

def register(request):
    if request.method == "POST":
        errors = Users.objects.basic_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/') 
        else:
            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            loginId = Users.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = hashpw)
            request.session['id'] = loginId.id
            return redirect('/groups')
    return redirect('/')

def login(request):
    if request.method == "POST":
        errors = {}
        info = Users.objects.filter(email = request.POST['email'])
        if len(info) == 0:
            errors["email"] = "Email not in database"
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                request.session['id'] = info[0].id
                return redirect('/groups')
            if not bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                errors['password'] = "wrong password"
                if len(errors) > 0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')
    return redirect('/')

def userHome(request):
    if 'id' not in request.session:
        return redirect('/')
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "groups": Groups.objects.all()
    }
    return render(request, "finalPythonAttemptApp/groups.html", context)

def addGroup(request):
    if request.method == "POST":
        errors = Groups.objects.basic_validator(request.POST)
        print(errors)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect("/groups")
        else:
            group = Groups.objects.create(name=request.POST['name'], description=request.POST['descrip'], createdBy=Users.objects.get(id=request.session['id']))
            group.members.add(request.session['id'])
            return redirect("/groups")
    return redirect("/groups")

def joinGroup(request, id):
    if request.method == "POST":
        thisGroup = Groups.objects.get(id=id)
        thisUser = Users.objects.get(id=request.session['id'])
        thisGroup.members.add(thisUser)
        return redirect("/usersGroup/" + str(id))
    return redirect("/usersGroup/" + str(id))

def leaveGroup(request, id):
    if request.method == "POST":
        thisGroup = Groups.objects.get(id=id)
        thisUser = Users.objects.get(id=request.session['id'])
        thisGroup.members.remove(thisUser)
        return redirect("/usersGroup/" + str(id))
    return redirect("/usersGroup/" + str(id)) 

def view(request, id):
    context = {
        "user": Users.objects.get(id = request.session['id']),
        "group": Groups.objects.get(id = id),
        "groups": Groups.objects.all()
    }
    return render(request, "finalPythonAttemptApp/group.html", context)

def destroy(request, id):
    if request.method == "POST":
        destroy = Groups.objects.get(id=id)
        destroy.delete()
        return redirect('/groups')
    return redirect('/groups')

def logout(request):
    request.session.flush()
    return redirect('/')