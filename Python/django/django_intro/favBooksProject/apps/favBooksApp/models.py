from __future__ import unicode_literals
from django.db import models
import re
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

class UsersManager(models.Manager):
    def basic_validator(self, postData):
        errors = {}
        if len(postData['first_name']) < 2:
            errors['first_name'] = "First name must be at least 2 characters long"
        if len(postData['last_name']) < 2:
            errors['last_name'] = "Last name must be at least 2 characters long"
        if len(postData['email']) < 2:
            errors['email'] = "Email must be at least 2 characters long"
        if len(postData['password']) < 8:
            errors['password'] = "Password must be at least 8 characters long"
        if(postData['confirm_password']) != postData['password']:
            errors['confirm_password'] = "Passwords must match"
        if not EMAIL_REGEX.match(postData['email']):
            errors['email'] = "Invalid Email"
        return errors

class Users(models.Model):
    first_name = models.TextField(max_length=45)
    last_name = models.TextField(max_length=45)
    email = models.TextField(max_length=45)
    password = models.TextField(max_length=45)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UsersManager()

class BooksManager(models.Manager):
    def basic_validator(self, postData):
        errors = {}
        if len(postData["title"]) == 0:
            errors["title"] = "Title required."
        if len(postData["descrip"]) < 5:
            errors["descrip"] = "Description must be at least 5 characters."
        return errors

class Books(models.Model):
    title = models.TextField(max_length=45)
    description = models.TextField(max_length=255)
    usersWhoLike = models.ManyToManyField(Users, related_name="likedBooks")
    uploadedBy = models.ForeignKey(Users, related_name="booksUploaded")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = BooksManager()