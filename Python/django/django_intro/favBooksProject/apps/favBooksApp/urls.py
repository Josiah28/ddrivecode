from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^books$', views.userHome),
    url(r'^addBook$', views.addBook),
    url(r'^addFavs/(?P<id>\d+)$', views.addFav),
    url(r'^unFav/(?P<id>\d+)$', views.unFav),
    url(r'^books/(?P<id>\d+)$', views.view),
    url(r'^update/(?P<id>\d+)$', views.update),
    url(r'^delete/(?P<id>\d+)$', views.destroy),
    url(r'^logout$', views.logout)
]