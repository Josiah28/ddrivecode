from django.shortcuts import render, redirect
from .models import Users
import bcrypt
from django.contrib import messages
from .models import UsersManager, Users, Books, BooksManager

def index(request):
    return render(request, "favBooksApp/index.html")

def register(request):
    if request.method == "POST":
        errors = Users.objects.basic_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/') 
        else:
            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            loginId = Users.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = hashpw)
            request.session['id'] = loginId.id
            return redirect('/books')
    return redirect('/')

def login(request):
    if request.method == "POST":
        errors = {}
        info = Users.objects.filter(email = request.POST['email'])
        if len(info) == 0:
            errors["email"] = "Email not in database"
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                request.session['id'] = info[0].id
                return redirect('/books')
            if not bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                errors['password'] = "wrong password"
                if len(errors) > 0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')
    return redirect('/')

def userHome(request):
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "books": Books.objects.all()
    }
    return render(request, "favBooksApp/books.html", context)

def addBook(request):
    if request.method == "POST":
        errors = Books.objects.basic_validator(request.POST)
        print(errors)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect("/books")
        else:
            fav_book = Books.objects.create(title=request.POST['title'], uploadedBy=Users.objects.get(id=request.session['id']), description = request.POST['descrip'])
            fav_book.usersWhoLike.add(request.session['id'])
            return redirect("/books")
    return redirect("/books")

def addFav(request, id):
    if request.method == "POST":
        thisBook = Books.objects.get(id=id)
        thisUser = Users.objects.get(id=request.session['id'])
        thisBook.usersWhoLike.add(thisUser)
        return redirect("/books")
    return redirect("/books")

def unFav(request, id):
    if request.method == "POST":
        thisBook = Books.objects.get(id=id)
        thisUser = Users.objects.get(id=request.session['id'])
        thisBook.usersWhoLike.remove(thisUser)
        return redirect("/books")
    return redirect("/books")

def view(request, id):
    context = {
        "user": Users.objects.get(id = request.session['id']),
        "book": Books.objects.get(id=id)
    }
    return render(request, "favBooksApp/book.html", context)

def update(request, id):
    if request.method == "POST":
        errors = Books.objects.basic_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
                return redirect('/books/' + str(id))
        else:
            updateBook = Books.objects.get(id=id)
            updateBook.title = request.POST['title']
            updateBook.description = request.POST['descrip']
            updateBook.save()
            return redirect('/books/' + str(id))
    return redirect('/books/' + str(id))

def destroy(request, id):
    if request.method == "POST":
        destroy = Books.objects.get(id=id)
        destroy.delete()
        return redirect('/books')
    return redirect('/books')

def logout(request):
    request.session.flush()
    return redirect('/')