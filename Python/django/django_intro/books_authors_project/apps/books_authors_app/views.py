from django.shortcuts import render, HttpResponse, redirect
from .models import Books
from .models import Authors

def authors(request):
    context = {
        "authors": Authors.objects.all()
    }
    return render(request, "books_authors_app/authors.html", context)

def addAuthor(request):
    if request.method == "POST":
        author_to_add = Authors.objects.create(first_name = request.POST['fname'], last_name = request.POST['lname'])
    return redirect('/authors')

def specificAuthor(request, id):
    authorInfo = Authors.objects.get(id=id)
    context = {
        "authorId": id,
        "firstName": authorInfo.first_name,
        "lastName": authorInfo.last_name,
        "books": authorInfo.books,
        "booksToAdd": Books.objects.all()
    }
    return render(request, "books_authors_app/author.html", context)

def addBooks(request, id):
    addBookInfo = Books.objects.get(id=id)
    context = {
        "id": id,
        "addBookTitle": addBookInfo.title
    }
    return render(request, "books_authors_app/author.html", context)

def books(request):
    context = {
        "books": Books.objects.all()
    }
    return render(request, "books_authors_app/books.html", context)

def addBook(request):
    if request.method == "POST":
        book_to_add = Books.objects.create(title = request.POST['title'], description = request.POST['descrip'])
    return redirect('/books')

def specificBook(request, id):
    bookInfo = Books.objects.get(id=id)
    context = {
        "bookId": id,
        "title": bookInfo.title,
        "descrip": bookInfo.description,
        "associatedAuthors": bookInfo.authors,
        "authors": Authors.objects.all()
    }
    return render(request, "books_authors_app/book.html", context)

def addAuthorToBook(request, id):
    if request.method = "POST":
        add_author = Authors.objects.get(id = request.POST["dropdown"])
    theBookInfo = Books.objects.get(id=id)
    this_book = Books.objects.get(id=id)
    this_author = authors.objects.get(id=id)
    this_book.authors.add(this_author)

def authorsOfBooks(request):
    context = {
        "authorsOfBooks": Books.authors.objects.all()
    }
    return render(request, "books_authors_app/book.html", context)