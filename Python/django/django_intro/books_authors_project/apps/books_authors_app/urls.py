from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^authors$', views.authors),
    url(r'^addauthor$', views.addAuthor),
    url(r'^(?P<id>\d+)$', views.specificAuthor),
     url(r'^(?P<id>\d+)/addbook$', views.addBookToAuthor),
    url(r'^books$', views.books),
    url(r'^addbook$', views.addBook),
    url(r'^book/(?P<id>\d+)$', views.specificBook),
    url(r'^book/(?P<id>\d+)/addauthor$', views.addAuthorToBook)
]