from django.db import models

class Books(models.Model):
    title = models.TextField(max_length=45)
    description = models.TextField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Authors(models.Model):
    first_name = models.TextField(max_length=45)
    last_name = models.TextField(max_length=45)
    books = models.ManyToManyField(Books, related_name="authors")
    notes = models.TextField(default="", max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)