from django.shortcuts import render, HttpResponse, redirect
import random
def index(request):
    if 'num_gold' not in request.session:
        request.session['num_gold'] = 0
    return render(request, "ninja_gold_app/ninjaGold.html")

def processMoney(request):
    if request.method == "POST":
        val_from_hidden = request.POST["location"]
        
    if request.POST['location'] == "farm":
        request.session['num_gold'] += random.randint(10,20)

    elif request.POST['location'] == "cave":
        request.session['num_gold'] += random.randint(5,10)

    elif request.POST['location'] == "house":
        request.session['num_gold'] += random.randint(2,5)

    elif request.POST['location'] == "casino":
        request.session['num_gold'] += random.randint(-50,50)
    else:
        print("You tried to change something!! Stop it.")
    return redirect('/')