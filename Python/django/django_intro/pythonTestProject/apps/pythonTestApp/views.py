from django.shortcuts import render, redirect
import bcrypt
from django.contrib import messages
from .models import UsersManager, Users, Quotes, QuotesManager

def index(request):
    return render(request, "pythonTestApp/index.html")

def register(request):
    if request.method == "POST":
        errors = Users.objects.basic_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/') 
        else:
            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            loginId = Users.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = hashpw)
            request.session['id'] = loginId.id
            return redirect('/quotes')
    return redirect('/')

def login(request):
    if request.method == "POST":
        errors = {}
        info = Users.objects.filter(email = request.POST['email'])
        if len(info) == 0:
            errors["email"] = "Email not in database"
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                request.session['id'] = info[0].id
                return redirect('/quotes')
            if not bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                errors['password'] = "wrong password"
                if len(errors) > 0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')
    return redirect('/')

def userHome(request):
    if 'id' not in request.session:
        return redirect('/')
    context = {
        "user": Users.objects.get(id=request.session['id']),
        "quotes": Quotes.objects.all()
    }
    return render(request, "pythonTestApp/quotes.html", context)

def addQuote(request):
    if request.method == "POST":
        errors = Quotes.objects.basic_validator(request.POST)
        print(errors)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect("/quotes")
        else:
            fav_quote = Quotes.objects.create(quote=request.POST['quote'], uploadedBy=Users.objects.get(id=request.session['id']))
            fav_quote.usersWhoLike.add(request.session['id'])
            return redirect("/quotes")
    return redirect("/quotes")

def addFav(request, id):
    if request.method == "POST":
        thisQuote = Quotes.objects.get(id=id)
        thisUser = Users.objects.get(id=request.session['id'])
        thisQuote.usersWhoLike.add(thisUser)
        return redirect("/quotes")
    return redirect("/quotes")

def unFav(request, id):
    if request.method == "POST":
        thisQuote = Quotes.objects.get(id=id)
        thisUser = Users.objects.get(id=request.session['id'])
        thisQuote.usersWhoLike.remove(thisUser)
        return redirect("/quotes")
    return redirect("/quotes")

def view(request, id):
    context = {
        "user": Users.objects.get(id = id),
        "quotes": Quotes.objects.get(id=id),
    }
    return render(request, "pythonTestApp/quote.html", context)

def viewEdit(request, id):
    context = {
        "user": Users.objects.get(id = request.session['id']),
    }
    return render(request, "pythonTestApp/editAccount.html", context)

def editAccount(request, id):
    if request.method == "POST":
        updateAccount = Users.objects.get(id=id)
        updateAccount.first_name = request.POST['first_name']
        updateAccount.last_name = request.POST['last_name']
        updateAccount.email = request.POST['email']
        updateAccount.save()
        return redirect('/account/' + str(id))
    return redirect('/account/' + str(id))

def destroy(request, id):
    if request.method == "POST":
        destroy = Quotes.objects.get(id=id)
        destroy.delete()
        return redirect('/quotes')
    return redirect('/quotes')

def logout(request):
    request.session.flush()
    return redirect('/')