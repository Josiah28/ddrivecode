from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^quotes$', views.userHome),
    url(r'^addQuote$', views.addQuote),
    url(r'^addFavs/(?P<id>\d+)$', views.addFav),
    url(r'^unFav/(?P<id>\d+)$', views.unFav),
    url(r'^usersQuotes/(?P<id>\d+)$', views.view),
    url(r'^account/(?P<id>\d+)$', views.viewEdit),
    url(r'^delete/(?P<id>\d+)$', views.destroy),
    url(r'^editAccount/(?P<id>\d+)$', views.editAccount),
    url(r'^logout$', views.logout)
]