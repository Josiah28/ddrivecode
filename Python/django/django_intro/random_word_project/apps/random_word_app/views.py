from django.shortcuts import render, HttpResponse, redirect
from django.utils.crypto import get_random_string
def index(request):
    random_string = get_random_string(length=32)
    if 'count' in request.session:
        request.session['count'] = request.session['count'] + 1
    else:
        request.session['count'] = 1
    request.session.random_string = random_string
    return render(request, "random_word_app/index.html")

def reset(request):
    request.session.clear()
    return redirect("/")