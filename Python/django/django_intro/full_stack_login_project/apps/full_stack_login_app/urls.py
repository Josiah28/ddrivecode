from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.forms),
    url(r'^registered$', views.register),
    url(r'^success$', views.userHome),
    url(r'^success$', views.login),
    url(r'^logout$', views.destroy)
]