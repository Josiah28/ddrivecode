from django.shortcuts import render, redirect
from django.contrib import messages
from .models import UserManager, User
import bcrypt

def forms(request):
    return render(request, "full_stack_login_app/full_stack_login_app.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.basic_validator(request.POST)

    if len(errors) > 0:
        for key, value in errors.items():
            messages.error(request, value)
        return redirect('/')
    
    else:
        hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
        loginId = User.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = hashpw)

        request.session['id'] = loginId.id

    return render(request, "full_stack_login_app/success.html")

def login(request):
    if request.method == "POST":
        errors = {}
        info = User.objects.filter(email = request.POST['email'])
        if len(info) == 0:
            return redirect('/success')
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                request.session['id'] = info[0].id
                return redirect('/success')
            if not bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                errors['password'] = "wrong password"
                if len(errors)>0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')

        return redirect('/success')

def userHome(request):
    context = {
        "user": User.objects.get(id = request.session['id'])
    }
    return render(request, "full_stack_login_app/success.html", context)

def destroy(request):
    request.session.flush()
    return redirect('/')