from django.apps import AppConfig


class FullStackLoginAppConfig(AppConfig):
    name = 'full_stack_login_app'
