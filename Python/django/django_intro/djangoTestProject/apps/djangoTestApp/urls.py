from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^thoughts$', views.userHome),
    url(r'^destroy$', views.destroy),
    url(r'^create$', views.createPost),
    url(r'^like$', views.likePost),
    url(r'^delete$', views.deletePost),
    url(r'^view$', views.view)
]