from django.shortcuts import render, redirect
from .models import User
import bcrypt
from django.contrib import messages
from .models import UserManager, User, Posts, PostManager

def index(request):
    return render(request, "djangoTestApp/index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.basic_validator(request.POST)

        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        
        else:
            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            loginId = User.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = hashpw)

            request.session['id'] = loginId.id

            return redirect('/thoughts')

def login(request):
    if request.method == "POST":
        errors = {}
        info = User.objects.filter(email = request.POST['email'])
        if len(info) == 0:
            return redirect('/thoughts')
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                request.session['id'] = info[0].id
                return redirect('/thoughts')
            if not bcrypt.checkpw(request.POST['password'].encode(),info[0].password.encode()):
                errors['password'] = "wrong password"
                if len(errors) > 0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')

        return redirect('/thoughts')

def userHome(request):
    context = {
        "user": User.objects.get(id=request.session['id']),
        "posts": Posts.objects.all()
    }
    return render(request, "djangoTestApp/dashboard.html", context)

def createPost(request):
    if request.method == "POST":
        errors = Posts.objects.basic_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect("/thoughts")
        else:
            post = Posts.objects.create(post=request.POST['postThought'], creator=User.objects.get(id=request.session['id']))
            return redirect("/thoughts")
    return redirect("/thoughts")

def likePost(request, id):
    if request.method == "POST":
        thisPost = Posts.objects.get(id=id)
        thisUser = User.objects.get(id=request.session['id'])
        thisPost.users_who_like.add(thisUser)
        return redirect("/thoughts")
    return redirect("/thoughts")

def deletePost(request, id):
    if request.method == "POST":
        thisPost = Posts.objects.get(id=id)
        thisPost.delete()
        return redirect("/thoughts")
    return redirect("/thoughts")

def destroy(request):
    request.session.flush()
    return redirect("/")

def view(request):
    return render(request, "djangoTestApp/view.html")