from flask import Flask, render_template, redirect, request, session, flash
from mysqlconnection import connectToMySQL
app = Flask(__name__)
app.secret_key = 'k+j=<3'

@app.route('/')
def start():
    return render_template("validation.html")

@app.route('/process', methods=['POST'])
def process():
    if len(request.form['first_name']) < 1:
        flash("Please enter a first name")

    if len(request.form['last_name']) < 1:
        flash("Please enter a last name")

    if len(request.form['comment']) < 2:
        flash("Comment should be at least 2 characters")

    if not '_flashes' in session.keys():
        mysql = connectToMySQL("validation")
        flash("Friend successfully added!")
        mysql = connectToMySQL("validation")
        query = "INSERT INTO dojos (first_name, last_name, location, favorite_language, created_at, updated_at) VALUES (%(fname)s, %(lname)s, %(local)s, %(flang)s, NOW(), NOW());"
        data = {
        "fname": request.form["first_name"],
        "lname": request.form["last_name"],
        "local": request.form["location"],
        "flang": request.form["favorite_language"],
    }
    validation = mysql.query_db(query, data)

    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)