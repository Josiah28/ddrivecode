from flask import Flask, render_template, request, redirect, session
app = Flask(__name__, static_url_path='/static')
app.secret_key = 'K + J = <3'

@app.route('/')
def counter():
    if 'count' in session:
        session['count'] = session['count'] + 1
    else:
        session['count'] = 1
    return render_template("counter.html")

@app.route('/reset')
def reset():
    session.clear()	
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)