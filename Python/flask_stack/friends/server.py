from flask import Flask, render_template, request, redirect
from mysqlconnection import connectToMySQL
app = Flask(__name__)

@app.route('/')
def friends():
    mysql = connectToMySQL("friends")
    friendsTable = mysql.query_db("SELECT * FROM friends;")
    print(friendsTable)
    return render_template("friends.html", all_friends = friendsTable)

@app.route("/create_friend", methods=["POST"])
def addFriend():
    mysql = connectToMySQL("friends")
    query = "INSERT INTO friends (first_name, last_name, occupation, created_at, updated_at) VALUES (%(fn)s, %(ln)s, %(occup)s, NOW(), NOW());"
    data = {
        "fn": request.form["fname"],
        "ln": request.form["lname"],
        "occup": request.form["occ"],
    }
    db = connectToMySQL("friends")
    mysql.query_db(query, data)
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)