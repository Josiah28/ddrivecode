from flask import Flask, render_template, request, redirect
from mysqlconnection import connectToMySQL
app = Flask(__name__)

@app.route('/')
def pets():
    mysql = connectToMySQL("pets")
    petsTable = mysql.query_db("SELECT * FROM pets;")
    print(petsTable)
    return render_template("pets.html", all_pets = petsTable)

@app.route("/create_pet", methods=["POST"])
def addPet():
    mysql = connectToMySQL("pets")
    query = "INSERT INTO pets (name, type, created_at, updated_at) VALUES (%(n)s, %(type)s, NOW(), NOW());"
    data = {
        "n": request.form["name"],
        "type": request.form["type"],
    }
    db = connectToMySQL("pets")
    mysql.query_db(query, data)
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)