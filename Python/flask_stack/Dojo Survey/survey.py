from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def survey():
    return render_template("survey.html")

@app.route('/result', methods=['POST'])
def survey2():
    print("Got Post Info")
    print(request.form)
    return render_template("survey2.html", name_on_template=request.form['name'], email_on_template=request.form['email'])

if __name__ == "__main__":
    app.run(debug=True)