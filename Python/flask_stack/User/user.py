from flask import Flask, render_template, request, redirect
from mysqlconnection import connectToMySQL
app = Flask(__name__)

@app.route('/')
def user():
    mysql = connectToMySQL("users")
    usersTable = mysql.query_db("SELECT * FROM users;")
    print(usersTable)
    return render_template("user.html", all_users = usersTable)

@app.route('/request', methods = ["POST"])
def createUser():
    print(request.form)
    mysql = connectToMySQL("users")
    query = "INSERT INTO users (name, email, created_at, updated_at) VALUES (%(n)s, %(em)s, NOW(), NOW());"
    data = {
        "n": request.form['name'],
        "em": request.form['email']
    }
    mysql.query_db(query, data)
    return redirect("/")

@app.route('/users/<id>')
def showUser(id):
    print(id)
    mysql = connectToMySQL("users")
    query = "SELECT  * FROM users WHERE idusers = %(id)s;"
    data = {
        "id": id
    }
    user = mysql.query_db(query, data)
    print(user)
    return render_template("showUser.html", user = user[0])

@app.route('/users/<id>/update')
def edit(id):
    mysql = connectToMySQL("users")
    query = "SELECT  * FROM users WHERE idusers = %(id)s;"
    data = {
        "id": id
    }
    user = mysql.query_db(query, data)
    return render_template("edit.html", user = user[0])

@app.route('/users/<id>/update/one', methods = ["POST"])
def update(id):
    mysql = connectToMySQL("users")
    query = "UPDATE users SET name = %(name)s, email = %(email)s, updated_at = NOW() WHERE idusers = %(id)s;"
    data = {
        "id": id,
        "name": request.form["name"],
        "email": request.form["email"],
    }
    user = mysql.query_db(query, data)
    return redirect('/')

@app.route('/users/<id>/destroy')
def destroy(id):
    mysql = connectToMySQL("users")
    query = "DELETE FROM users WHERE idusers = %(id)s;"
    data = {
        "id": id
    }
    user = mysql.query_db(query, data)
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)