from flask import Flask, render_template, request, redirect, session
app = Flask(__name__)
app.secret_key = 'K + J = <3'
import random

@app.route('/', methods = ['GET'])
def correctNum():
    if 'number' in session:
        pass
    else:
        session['number'] = random.randint(1,100)
    print(session['number'])
    return render_template("numberGame.html")

@app.route('/result', methods = ['POST'])
def result():
    if int(request.form['guess']) > session['number']:
        print("Your guess was too high!")
        session["guess"] = "Your guess was too high!"
    if int(request.form['guess']) < session['number']:
        print("Your guess was too low!")
        session["guess"] = "Your guess was too low!"
    if int(request.form['guess']) == session['number']:
        session["guess"] = "That's the number!"
    return redirect('/')

@app.route('/reset')
def reset():
    session.clear()
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)