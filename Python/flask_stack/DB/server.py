from flask import Flask, render_template, request, redirect
from mysqlconnection import connectToMySQL
app = Flask(__name__)   

@app.route('/')          
def index():
	mysql = connectToMySQL('mydb')	        # call the function, passing in the name of our db
	users = mysql.query_db('SELECT * FROM users;')  # call the query_db function, pass in the query as a string
	print(users)
	return render_template("index.html", all_users = users)

@app.route('/request', methods = ["POST"])
def createUser():
	print(request.form)
	mysql = connectToMySQL('mydb')
	query = "INSERT INTO users (name, email, created_at, updated_at) VALUES (%(n)s, %(em)s, NOW(), NOW());"
	data = {
		"n": request.form['name'],
		"em": request.form['email']
	}
	mysql.query_db(query, data)
	return redirect("/")

if __name__ == "__main__":   
	app.run(debug=True)   