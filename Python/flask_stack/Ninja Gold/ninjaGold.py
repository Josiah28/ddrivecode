from flask import Flask, render_template, request, redirect, session
import random
app = Flask(__name__)
app.secret_key = 'k+j=<3'

@app.route('/')
def index():
    if 'num_gold' not in session:
        session['num_gold'] = 0
    return render_template("ninjaGold.html")

@app.route('/process_money', methods = ["POST"])
def processMoney():
    if request.form['location'] == "farm":
        session['num_gold'] += random.randint(10,20)

    elif request.form['location'] == "cave":
        session['num_gold'] += random.randint(5,10)

    elif request.form['location'] == "house":
        session['num_gold'] += random.randint(2,5)

    elif request.form['location'] == "casino":
        session['num_gold'] += random.randint(-50,50)
    else:
        print("You tried to change something!! Stop it.")
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)