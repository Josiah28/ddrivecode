from flask import Flask, render_template, request, redirect, session, flash
from mysqlconnection import connectToMySQL
import re
app = Flask(__name__)
app.secret_key = "k+j=<3"
from flask_bcrypt import Bcrypt        
bcrypt = Bcrypt(app)

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

@app.route('/')
def logre():
    return render_template("login_registration.html")

@app.route('/register', methods = ["POST"])
def register():
    if len(request.form['first_name']) < 1:
        flash("Please enter a first name")

    if len(request.form['last_name']) < 1:
        flash("Please enter a last name")

    if len(request.form['email']) < 2:
        flash("Invalid Email")
    
    if len(request.form['password']) < 8:
        flash("Password must be at least 8 characters long")

    if (request.form['confirm_password'] != request.form['password']):
        flash("Passwords must match")

    if not EMAIL_REGEX.match(request.form['email']):
        flash("Invalid email address!")

    if not '_flashes' in session.keys():
        print("got here")
        pw_hash = bcrypt.generate_password_hash(request.form['password'])
        mysql = connectToMySQL("login")
        query = "INSERT INTO logins (first_name, last_name, email, password, confirm_password, created_at, updated_at) VALUES (%(fn)s, %(ln)s, %(em)s, %(p)s, %(cp)s, NOW(), NOW());"
        data = {
            "fn": request.form['first_name'],
            "ln": request.form['last_name'],
            "em": request.form['email'],
            "p": pw_hash,
            "cp": pw_hash,
        }
        registered = mysql.query_db(query, data)

        flash("Register Successful!")
        session['id'] = registered
        return redirect("/success")
    else:
        return redirect('/')

@app.route('/login', methods = ["POST"])
def login():
    if len(request.form['email']) < 2:
        flash("You could not be logged in")
    
    if len(request.form['password']) < 8:
        flash("You could not be logged in")

    if not '_flashes' in session.keys():
        return redirect("/success")
    return redirect('/')

    mysql = connectToMySQL("login")
    query = "SELECT * FROM logins WHERE password = %(p)s;"
    data = { "password" : request.form["password"] }
    result = mysql.query_db(query, data)
    registry = mysql.query_db(query, data)
    if len(result) > 0:
        if bcrypt.check_password_hash(result[0]['password'], request.form['password']):
            session['id'] = result[0]['id']
            return redirect('/success')
    else:        
        flash("You could not be logged in")
        return redirect("/")

@app.route('/success')
def success():
    if 'id' in session:
        return render_template("successful.html")
    else:
        return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)