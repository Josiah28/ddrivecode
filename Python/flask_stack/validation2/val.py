from flask import Flask, render_template, request, redirect, flash, session
from mysqlconnection import connectToMySQL
import re
app = Flask(__name__)
app.secret_key = "k+j=<3"

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

@app.route('/')
def email():
    mysql = connectToMySQL("email")
    emailsTable = mysql.query_db("SELECT * FROM emails;")
    print(emailsTable)
    return render_template("val.html", all_emails = emailsTable)

@app.route('/request', methods = ["POST"])
def createEmail():
    mysql = connectToMySQL("email")
    query = "INSERT INTO emails (email, created_at, updated_at) VALUES (%(em)s, NOW(), NOW());"
    data = {
        "em": request.form['Enter_Email']
    }
    mysql.query_db(query, data)
    if not EMAIL_REGEX.match(request.form['Enter_Email']):
        flash("Invalid email address!")
        return redirect("/")
    else:
        return redirect("/success")

@app.route('/success', methods = ["GET"])
def successPage():
    mysql = connectToMySQL("email")
    query = "SELECT * FROM emails;"
    x = mysql.query_db(query)
    return render_template("success.html", all_emails = x)

if __name__ == "__main__":
    app.run(debug=True)