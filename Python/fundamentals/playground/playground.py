from flask import Flask, render_template
app = Flask(__name__)
@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/play')
def play():
    return render_template('playground.html', times = 3,  color = 'cyan')

@app.route('/play/<times>')
def multiple(times):
    return render_template('playground.html', times = int(times), color = 'cyan')

@app.route('/play/<times>/<color>')
def shades(times,color):
    return render_template('playground.html', times = int(times), color = color)

if __name__=="__main__":
    app.run(debug=True)