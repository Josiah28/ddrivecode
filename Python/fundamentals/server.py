from flask import Flask, render_template, request
app = Flask(__name__)
app.secret_key = 'Kenna + Josiah = <3'

@app.route('/')          
def board_games():
	return 'Welcome to the board game center!'

@app.route('/monopoly')
def monopoly():
	return 'Ruining families and friend circles since 1903!'

@app.route('/form', methods = ['GET']) #methods = ['GET'] (and only 'GET') is optional
def formget():
	return render_template("form.html")

@app.route('/request', methods = ["POST"])
def showRequest():
	print("POST route", request.form)
	session['game'] = request.form["game"]
	return redirect('/thankyou')

@app.route('/thankyou')
def thankyou():
	print("GET route", request.form)
	return render_template("request.html")

if __name__ == "__main__":   
	app.run(debug=True)   