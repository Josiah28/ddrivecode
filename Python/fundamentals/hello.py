from flask import Flask  # Import Flask to allow us to create our app
app = Flask(__name__)    # Create a new instance of the Flask class called "app"
@app.route('/')          # The "@" decorator associates this route with the function immediately following
def hello_world():
    return 'Hello World!'  # Return the string 'Hello World!' as a response


@app.route('/dojo') # for a route '/hello/____' anything after '/hello/' gets passed as a variable 'name'
def hello():
    return "Dojo!"

@app.route('/say') # for a route '/hello/____' anything after '/hello/' gets passed as a variable 'name'
def say():
    return "Hi"

@app.route('/say/flask') # for a route '/hello/____' anything after '/hello/' gets passed as a variable 'name'
def flask():
    return "Hi Flask"

@app.route('/say/josiah') # for a route '/hello/____' anything after '/hello/' gets passed as a variable 'name'
def josiah():
    return "Hi Josiah"

@app.route('/say/kenna') # for a route '/hello/____' anything after '/hello/' gets passed as a variable 'name'
def kenna():
    return "Hi Kenna <3"

@app.route('/repeat/<x>/<num>')
def repeat(x,num):
    y = int(x)
    temp = y*num
    return temp

if __name__=="__main__":   # Ensure this file is being run directly and not from a different module    
    app.run(debug=True)    # Run the app in debug mode.