class BankAccount
    attr_accessor(:num, :checkingTotal, :savingsTotal, :numOfAccounts, :intRate)
    @@numOfAccounts = 0
    @intRate = 0.01
  
    def initialize
      num = rand(1987532..9990128)
      @checkingTotal = 0
      @savingsTotal = 0
      @num = num
      @@numOfAccounts += 1
    end
  
    def displayAccounts
      puts @@numOfAccounts
      self
    end
  
    def accountInfo
      puts @num
      puts total
      puts @checkingTotal
      puts @savingsTotal
      puts @intRate
      self
    end
  
    def displayInt
      puts @intRate
      self
    end
  
    def total
      total = @checkingTotal + @savingsTotal
      puts total
      self
    end
  
    def displayChecking
      puts @checkingTotal
      self
    end
  
    def displaySavings
      puts @savingsTotal
      self
    end
  
    def checkingDeposit(cDep)
      @checkingTotal += cDep
      self
    end
  
    def savingsDeposit(sDep)
      @savingsTotal += sDep
      self
    end
  
    def checkingWithdrawal(cWit)
      if checkingTotal < cWit
        puts "Insufficient Funds"
      else
      @checkingTotal -= cWit
      end
      self
    end
  
    def savingsWithdrawal(sWit)
      if savingsTotal < sWit
        puts "Insufficient Funds"
      else
        @savingsTotal -= sWit
      end
      self
    end
end
me = BankAccount.new
puts me.checkingDeposit(20).savingsDeposit(100).accountInfo