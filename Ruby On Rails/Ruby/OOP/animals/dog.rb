require_relative 'mammal'
class Dog < Mammal
    def subclass_method
        breath
    end
    def another_method
        self.eat
    end
end
human = Human.new