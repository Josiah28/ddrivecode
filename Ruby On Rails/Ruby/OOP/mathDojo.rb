#Challenge 1
class MathDojo
    def initialize
    @sum=0
    end
    def add num1, num2=0
    puts @sum
        @sum+=num1 + num2
        self
    end
    def subtract num1, num2= 0
    puts @sum
        @sum -=num1 + num2
        self
    end
    def result
      return @sum
    end
end
challenge1 = MathDojo.new.add(2).add(2, 5).subtract(3, 2).result # => 4

#Challenge 2
class MathDojo
    def initialize
        @sum=0
    end
    def add num1, num2=0
        if num1.class == Array
            for i in 0...num1.count()
                @sum += num1[i]
            end
        else
            @sum += num1
        end
        if num2.class == Array
            for i in 0...num2.count()
                @sum += num2[i]
            end
        else
            @sum += num2
        end
        self
    end
    def subtract num1, num2=0
        if num1.class == Array
            for i in 0...num1.count()
                @sum -= num1[i]
            end
        else
            @sum -= num1
        end
        if num2.class == Array
            for i in 0...num1.count()
                @sum -= num2[i]
            end
        else
            @sum -= num2
        end
        self
    end
    def result
        return @sum
    end
end
challenge2 = MathDojo.new.add(1).add([3, 5, 7, 8], [2, 4.3, 1.25]).subtract([2,3], [1.1, 2.3]).result