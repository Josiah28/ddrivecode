require_relative 'mammal'
class Lion < Mammal
    def initialize
        super(170)
        puts "This is a lion"
    end
    def fly
        @health -= 10
        self
    end
    def attackTown
        @health -= 50
        self
    end
    def eatHumans
        @health += 20
        self
    end
end
lion = Lion.new
puts lion.attackTown.attackTown.attackTown.eatHumans.eatHumans.fly.fly.displayHealth