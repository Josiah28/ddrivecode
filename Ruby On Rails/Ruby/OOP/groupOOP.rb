class Ninja
    attr_accessor :name, :belts, :dojo
    def initialize(name, belts, dojo)
        @name = name
        @belts = belts
        @dojo = dojo
    end
    def displayAll
        puts "Name: #{name},  Belts: #{belts},  Location: #{dojo}"
        self
    end
    def upgradeBelt(color)
      @belts = color
      puts @belts
      self
    end
    def changeDojo(location)
      puts "We are changing from #{@dojo} to #{location}"
      @dojo = location
      puts @dojo
      self
    end
end
ninja1 = Ninja.new("Josiah", "Black Belt", "Dallas").displayAll.upgradeBelt("Red Belt").changeDojo("Los Angeles")