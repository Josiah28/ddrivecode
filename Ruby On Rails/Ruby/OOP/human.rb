class Human
    attr_accessor(:strength, :intelligence, :stealth, :health)
    def initialize
      @strength = 3
      @intelligence = 3
      @stealth = 3
      @health = 100
    end
  
    def attack(target)
      attack = 10
      target.health -= attack
      self
    end
  
    def displayHealth
      puts @health
      self
    end
end
human1 = Human.new
human2 = Human.new
puts human1.attack(human2)
puts human2.displayHealth