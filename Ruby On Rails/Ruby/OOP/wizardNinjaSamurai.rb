class Human
    attr_accessor(:strength, :intelligence, :stealth, :health)
    def initialize
      @strength = 3
      @intelligence = 3
      @stealth = 3
      @health = 100
    end
  
    def attack(target)
      attack = 10
      target.health -= attack
      self
    end
  
    def displayHealth
      puts @health
      self
    end
  end
  human1 = Human.new
  human2 = Human.new
  puts human1.attack(human2)
  puts human2.displayHealth
  
  class Wizard < Human
    def initialize
      super(3, 25, 3, 50)
    end
  
    def heal
      @health += 10
      self
    end
  
    def fireBall(target)
      attack = 20
      target.health -= attack
      self
    end
  end
  
  class Ninja < Human
    def initialize
      super(3, 3, 175, 3)
    end
  
    def steal
      @health += 10
      self
    end
  
    def getAway
      @health -= 15
      self
    end
  end
  
  class Samurai < Human
    attr_accessor(:numOfSamurai)
    @@numOfSamurai = 0
    def initialize
      super(3, 3, 3, 200)
      @@numOfSamurai += 1
    end
  
    def deathBlow(target)
      target.health = 0
      self
    end
  
    def meditate
      @health = 200
      self
    end
end