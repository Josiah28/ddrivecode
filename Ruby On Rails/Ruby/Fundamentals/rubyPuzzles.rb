# 1.
def sumGt
  arr = [3,5,1,2,7,9,8,13,25,32]
  arr2 = []
  sum = 0
  for i in arr
    sum += arr[i]
    if arr[i] > 10
      arr2.push(arr[i])
    end
  end
  puts sum
  arr2
end
sumGt

# 2.
def shuffle
    arr = ['John', 'KB', 'Oliver', 'Cory', 'Matthew', 'Christopher']
    temp =[]
    arr = arr.shuffle
    for i in 0...arr.count
      puts arr[i]
      if arr[i].length > 5
        temp.push(arr[i])
      end
    end
    return temp
end
shuffle

# 3.
def alpha
  arr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
  arr2 = ['a', 'e', 'i', 'o', 'u', 'y']
  arr = arr.shuffle
  puts arr[arr.length-1]
  puts arr[0]
  for i in 0...arr2.count
    puts "This is a vowel" if arr2[i] == arr[0]
  end
end
alpha

# 4. Random int array generator
def randomIntArray
  arr = []
  for i in (1..10)
      arr.push(rand(55..100))
  end
  return arr
end
puts randomIntArray

# 5. Print them in order and then min and max
def sortedMM
  arr = []
  for i in (1..10)
      arr.push(rand(55..100))
  end
  return arr.sort.push(arr.min()).push(arr.max())
end
puts sortedMM

# 6. Random String
def randomString
  str = ""
  for i in (0...5)
      str += (65+rand(26)).chr
  end
  return str
end
puts randomString

# 7. Generate an array with 10 random strings that are each 5 characters long
def randomStringArray
  str=""
  arr=[]
  for x in (1..10)
      for i in (0...5)
          str+=(65+rand(26)).chr
      end
      arr.push(str)
      str=""
  end
  return arr
end

puts randomStringArray