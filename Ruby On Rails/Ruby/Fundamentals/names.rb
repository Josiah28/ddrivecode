def namesFunc
  a = {first_name: "Josiah", last_name: "Jamison"}
  b = {first_name: "Kenna", last_name: "West"}
  c = {first_name: "Malik", last_name: "Etienne"}
  d = {first_name: "RanD", last_name: "Hedrick"}
  e = {first_name: "Gavin", last_name: "Weatherly"}
  names = [a, b, c, d, e]
  count = 0
  for i in 0...names.count
      count += 1
      puts names[i]
  end
  puts "You have #{count} people in the 'names' array."
end
namesFunc