a = [2, 4, 3, 7, 9, 4, 6]
b = ["Josiah", "Kenna", "Malik", "RanD"]

puts "Class name: #{a.class}"
puts "It does include 3!" if a.include? 3
puts "The last number of this range is " + a.last.to_s
puts "The maximum number of this range is " + a.max.to_s
puts "The minimum number of this range is " + a.min.to_s
puts b.shuffle.to_s