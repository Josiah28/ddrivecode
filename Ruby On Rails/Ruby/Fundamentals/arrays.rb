a = [2, 4, 3, 7, 9, 4, 6]
b = ["Josiah", "Kenna", "Malik", "RanD"]
c = a + b

puts c.shuffle.join(', ')
c.delete('Kenna')

puts c

puts "The length of a is #{a.length}"

d = %w{Josiah Kenna Malik}
puts d.values_at(1,2).join(' and ')

e = %w{ 1 2 3 }
puts e.reverse

a.at(1)
puts a.slice(2)