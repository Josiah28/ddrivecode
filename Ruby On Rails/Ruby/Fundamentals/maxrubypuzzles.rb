
Create an array with the following values: 3,5,1,2,7,9,8,13,25,32. Print the sum of all numbers in the array. Also have the function return an array that only include numbers that are greater than 10 (e.g. when you pass the array above, it should return an array with the values of 13,25,32 - hint: use reject or find_all method)


arr = [3, 5, 1, 2, 7, 9, 8, 13, 25, 32]
puts arr.sum
puts arr.reject{|i| i<10 }


Create an array with the following values: John, KB, Oliver, Cory, Matthew, Christopher. Shuffle the array and print the name of each person. Have the program also return an array with names that are longer than 5 characters.

arr = ["John", "KB", "Oliver", "Cory", "Matthew", "Christopher"]
print arr.shuffle
print arr.select{|i| i.length > 5}


Create an array that contains all 26 letters in the alphabet (this array must have 26 values). Shuffle the array and display the last letter of the array. Have it also display the first letter of the array. If the first letter in the array is a vowel, have it display a message.

def shuffleVowels arr
    shuffled= arr.shuffle()
    puts shuffled.last()
    if shuffled[0] == ("a" or "e" or "i" or "o" or "u" or "y")
      puts shuffled
      puts "the first letter is a vowel"
    end
end
puts shuffleVowels ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

#random integer array
def randomIntArray
    arr = []
    for i in (1..5)
        arr.push(rand(55..100))
    end
    return arr
end
puts randomIntArray

#sorted min max
def sortedMM
    arr = []
    for i in (1..10)
        arr.push(rand(55..100))
    end
    return arr.sort.push(arr.min()).push(arr.max())
end
puts sortedMM

#return random string
def randomString
    str=""
    for i in (0...5)
        str+=(65+rand(26)).chr
    end
    return str
end
puts randomString

#5 string array
def randomStringArray
    str=""
    arr=[]
    for x in (1..10)
        for i in (0...5)
            str+=(65+rand(26)).chr
        end
        arr.push(str)
        str=""
    end
    return arr
end

puts randomStringArray