#  1. Print 1-255
def twoFiftyFive
    for i in 0..255
        puts i
    end
end
twoFiftyFive

# 2 .Print odd numbers between 1-255
def odd
    for o in 0..255
        if o % 2 == 1
            puts o
        end
    end
end
odd

# 3 .Sum
def sum
    sum = 0
    for s in 0..255
        sum += s
        puts "New Number: #{s}" + " Sum: #{sum}"
    end
end
sum

# 4. Loop through array
def loop
    arr = [1, 2, 3, 4, 5]
    for l in arr
      puts l
    end
end
loop

# 5. Max
x = [-3, -5, -7]
puts x.max

# 6. Avg
x = [2, 10, 3]
puts x.sum.to_f / x.length

# 7. Array with odd numbers
y = []
for x in 1..255 
y.push(x) if x % 2 !=0
end
print y


x = (1..255).step(2).to_a
print x

# 8. Greater than y
y= 3
count = 0
arr = [1, 3, 5, 7]
for x in 0...arr.count
  if arr[x] > y
  count+=1
end
end
puts count

# 9. Square the values
x = [1, 5, 10, -2]
for i in 0...x.count()
    x[i] *= x[i]
end
print x

# 10. Eliminate negative numbers
x = [1, 5, 10, -2]
for i in 0...x.count
if x[i] < 0
  x[i] = 0
end
end
puts x

print x.select{|i| i > 0}

# 11. Max, Min, & Avg
x = [1, 5, 10, -2]
puts x.max, x.min, x.sum/x.length

# 12. Shifting the Values in the Array
x = [1, 5, 10, 7, -2]
for i in 0...x.count
    x[i] = x[i+1]
end
x.pop
x.push(0)

# 13. Number to String
x = [-1, -3, 2]
for i in 0...x.count
x[i] = 'Dojo' if x[i] < 0
end
puts x