# delete
h = {first_name: "Coding", last_name: "Dojo"}
h.delete(:last_name)
puts h

# empty?
h = {}
a = h.empty?
puts a

# has key
h = {first_name: "Coding", last_name: "Dojo"}
a = h.has_key?(:last_name)
puts a

# has value
h = {first_name: "Coding", last_name: "Dojo"}
a = h.has_value?("Dojo")
puts a