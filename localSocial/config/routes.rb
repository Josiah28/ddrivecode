Rails.application.routes.draw do
  root 'sessions#new' # Renders Log/Reg Page
  post 'users/login' => 'sessions#create' # Login
  post 'users/new' => 'users#create' # Register
  delete 'sessions/:user_id' => 'sessions#destroy' # Logout
  
  get 'posts' => 'posts#lsFeed' # Renders Posts Page
  post 'createpost' => 'posts#create' # Creates Post
  get 'posts/:post_id' => 'posts#postInfo' # Renders Post Info Page
  post 'like/:post_id' => 'posts#like' # Likes Post
  delete 'unlike/:post_id' => 'posts#unlike' # Unlikes Post
  delete "posts/:post_id" => "posts#delete" # Deletes Post
  get "posts/:post_id/edit" => "posts#editPost" # Renders Edit Page
  patch "posts/:post_id/edit" => "posts#update" # Edits Post

  get 'users/:user_id' => 'users#profilePage' # Renders Users Page
  get "users/:user_id/edit" => "users#editUser" # Renders Edit Page
  patch "users/:user_id/edit" => "users#update" # Edits User
end