class UsersController < ApplicationController
    skip_before_action :check, only: [:create]
    def create
      user = User.create(user_params)
      if user.valid?
        session[:user_id] = user.id
        redirect_to '/posts'
      else
        flash[:errors] = user.errors.full_messages
        redirect_to '/'
      end
    end
  
    def profilePage
      @user = User.find(params[:user_id])
      @posts = Post.where(user: User.find(params[:user_id]))
      @likes = Like.where(post: @posts)
    end

    def editUser
      @user = User.find(params[:user_id])
    end

    def update
        @user = User.find(params[:user_id])
        @update = @user.update(userUpdate_params)
        redirect_to '/posts'
    end
  
    private 
      def user_params
        params.require(:user).permit(:firstName, :lastName, :alias, :email, :password, :password_confirmation)
      end

      def userUpdate_params
        params.require(:user).permit(:firstName, :lastName, :alias)
      end
end