class PostsController < ApplicationController
    def lsFeed
        @user = current_user
        @posts = Post.all
    end

    def create
        @post = Post.create( post_params )
        if @post.valid?
            redirect_to '/posts'
        else
            flash[:errors] = @post.errors.full_messages
            redirect_to '/posts'
        end
    end

    def postInfo
        @post = Post.find(params[:post_id])
        @users = User.all
    end

    def like
        if Post.find_by(id: params[:post_id])
            @post = Post.find(params[:post_id])
            if not @post.usersWhoLiked.include?(current_user)
                Like.create(user: User.find(session[:user_id]), post: Post.find(params[:post_id]))
            end
        end
        redirect_to :back
    end

    def unlike
        @like = Like.where(user: User.find(session[:user_id]), post: Post.find(params[:post_id]))
        @like.destroy_all
        redirect_to :back
    end

    def delete
        if Post.find_by(id: params[:post_id])
            @postDelete = Post.find(params[:post_id])
            if @postDelete.user_id == current_user.id
                @postDelete.destroy
            end
        end
        redirect_to :back
    end

    def editPost
        @post = Post.find(params[:post_id])
    end

    def update
        @post = Post.find(params[:post_id])
        @update = @post.update(post_params)
        redirect_to '/posts'
    end

    private
        def post_params
            params.require(:post).permit(:content).merge(user: User.find(current_user))
        end
end