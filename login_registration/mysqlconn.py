import pymysql.cursors
class MySQLConnection:
    def __init__(self, db):
        connection = pymysql.connect(host = 'localhost',
                                    user = 'root', #change user recive a runtimeerror
                                    password = 'root', #change pass recieve a runtimeerror
                                    db = db, #change db recieve a nameerror
                                    charset = 'utf8mb4', #chang charset recieve attribute error
                                    cursorclass = pymysql.cursors.DictCursor, #attribute error
                                    autocommit = True) #name error
        self.connection = connection
    def query_db(self, query, data=None):
        with self.connection.cursor() as cursor:
            try:
                query = cursor.mogrify(query, data)
                print("Running Query:", query)
     
                executable = cursor.execute(query, data) 
                if query.lower().find("insert") >= 0:
                    # if the query is an insert, return the id of the last row, since that is the row we just added
                    self.connection.commit() #changed but no error
                    return cursor.lastrowid
                elif query.lower().find("select") >= 0:

                    result = cursor.fetchall()
                    return result
                else:

                    self.connection.commit()
            except Exception as e:

                print("Something went wrong", e)
                return False
            finally:

                self.connection.close() 

def connectToMySQL(db):
    return MySQLConnection(db)