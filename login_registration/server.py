from flask import Flask, render_template, redirect, request, flash, session
from mysqlconn import connectToMySQL
import re
app=Flask(__name__)
app.secret_key='mystery'
from flask_bcrypt import Bcrypt        
bcrypt = Bcrypt(app)

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
name_REGEX = re.compile(r'^[a-zA-Z]+$')



@app.route('/')
def form():
    return render_template('index.html')

@app.route('/register', methods=['POST'])
def register():
    is_valid=True
    
    if len(request.form['fname'])<1:
        is_valid=False
        flash("please enter a first name")
    if not name_REGEX.match(request.form['fname']):
         is_valid=False
         flash("you must only enter letters")

    if len(request.form['lname'])<1:
        is_valid=False
        flash("please enter a last name")

    if not name_REGEX.match(request.form['fname']):
         is_valid=False
         flash("you must only enter letters")

    if len(request.form['ename'])<1:
        is_valid=False
        flash("please enter email address")

    if len(request.form['pname'])<1:
        is_valid=False
        flash("please enter a password")

    if not EMAIL_REGEX.match(request.form['ename']):
        is_valid=False
        flash("Invalid email address!")

    if (request.form['pname'] != request.form['pcname']):
        is_valid=False
        flash("password must match")
    
    if not is_valid: 
        return redirect('/')
    else:
        pw_hash = bcrypt.generate_password_hash(request.form['pname'])

        mysql = connectToMySQL("login_db")
        query = "INSERT INTO registrations (first_name, last_name, email, password, created_at, updated_adt) VALUES (%(fn)s, %(ln)s, %(em)s, %(pn_hash)s, Now(),Now());"
        print("got here")
        data={
        "fn": request.form["fname"],
        "ln": request.form["lname"],
        "em": request.form["ename"],
        "pn_hash": pw_hash
        }
        reg=mysql.query_db(query,data)
        flash("Register Successful!")
        session['id'] = reg
        return redirect("/success")

    return redirect('/')

@app.route('/login', methods = ["POST"])
def login():
    is_valid=True
    mysql=connectToMySQL('login_db')
    query= "SELECT * FROM registrations WHERE email=%(em)s "
    data={
    'em':request.form['ename']
    }
    emails = mysql.query_db(query,data)
    if len(emails)==0:
        flash('you could not be logged in')
        return redirect('/')
    else:
        if bcrypt.check_password_hash(emails[0]['password'], request.form['password']):
            return redirect('/success')
        else:
            flash("not logged in")
            return redirect('/')




 
#     if len(request.form['password']) < 8:
#         flash("You could not be logged in")



#     mysql = connectToMySQL("login_db")
#     query = "SELECT * FROM logins WHERE password = %(pw)s;"
#     data = {
        
#         "pw": request.form['pname']
#     }
#     result = mysql.query_db(query, data)
#     registry = mysql.query_db(query, data)
#     if len(result) > 0:
#         if bcrypt.check_password_hash(result[0]['password'], request.form['password']):
#             session['id'] = result[0]['id']
#             return redirect('/success')


@app.route('/success')
def success():
    if 'id' in session:
        return render_template("success.html")
    else:
        return redirect('/')






























if __name__=="__main__":
    app.run(debug=True)