var express = require('express'),
    app = express(),
    port = 1400;

app.use(express.static( __dirname + '/public/dist/public' ));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.listen(1400, function () {
    console.log(`listening on port ${port}`);
})