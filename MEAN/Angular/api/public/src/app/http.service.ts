import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) {
    this.getPokemon();
   }
   getPokemon(){
    let squirtle = this._http.get('https://pokeapi.co/api/v2/pokemon/7/');
    squirtle.subscribe(data => {
      console.log("Got our favorite pokemon!", data["forms"][0].name);
      console.log("Got one of our favorite pokemon's ability!", data["abilities"][0].ability.name);
      let squirtlesAbility = this._http.get('https://pokeapi.co/api/v2/ability/' + data["abilities"][0].ability.name);
      squirtlesAbility.subscribe(response => {
        console.log(response);
        console.log(response["pokemon"].length-1 + " " + "other Pokemon share squirle's ability 'rain-dish'.");
      })
    })
  }
}