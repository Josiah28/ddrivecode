class Bike {
    constructor(
       public price: number,
       public maxSpeed: string,
       public miles = 0
       ){
          function displayInfo(){
              console.log("Price:" + this.price + " " + "Max Speed:" + this.maxSpeed + "Miles:" + " " + miles);
              return this;
          } // display nested function
          function ride(){
              this.miles += 10;
              console.log("Riding...");
              return this;
          }
          function reverse(){
              this.miles -= 5;
              return this;
          }
       } // constructor function
}
const bike1 = new Bike(100, "15mph");
const bike2 = new Bike(200, "25mph");
const bike3 = new Bike(300, "35mph");
console.log(bike1.ride().ride().ride().reverse().dislpayInfo());
console.log(bike2.ride().ride().ride().reverse().reverse().dislpayInfo());
console.log(bike3.ride().reverse().reverse().reverse().dislpayInfo());