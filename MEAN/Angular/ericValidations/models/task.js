var mongoose = require('mongoose');
const Schema = mongoose.Schema

var ProductSchema = new mongoose.Schema({
    name: {type: String, default: "", required: [true, "product must have name"], minlength: [3, "must be at least 3 characters"]},
    quantity:{type: Number, default: 0,required:[true, "quantity must be filled"],min: [0, "quantity must be positive"]},  
    price:{type: Number, default: 0,required:[true, "price must be filled"],min: [0,"price must be positive"]},  

}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Product", ProductSchema);


