var mongoose = require("mongoose");
var Product = mongoose.model("Product");


module.exports = {
    products: function(req, res){
        Product.find({}, function(err,products){
            if(err){
                res.json({message: "Error", error: err});
            }
            else{
                res.json(products);
                console.log("end of products on controller")
            }
        })
    },
    details: function(req, res){
        console.log("in details")
        let id = req.params.id;
        Product.findOne({_id: id},function(err, product){
            if(err){
                console.log("here in error")
                res.json({message: "error", error: err});
            }
            else{
                console.log(product)
                res.json({message: "success", product:product});
            }
        });
    },
    addProduct: function(req, res){
        Product.create({name: req.body.name, quantity: req.body.quantity, price: req.body.price},function(err,product){
            if(err){
                res.json({message: "Error with addProduct",error:err});
            }
            else{
                res.json({message:"Success!",added:true})

            }
        })
    },

    deleteProduct: function(req, res){
        console.log("inside deleteMovie controller")
        let id = req.params.id;
        Product.remove({_id: id},function(err){
            if(err){
                res.json({message: "Error!", error: err});
            }
            else{
                res.json({message: "Success!", removed: true});
            }
        })
    },
    editProduct: function(req, res){
        let id = req.params.id;
        console.log(id)
        Product.findById(id, function(err, product){
            if(err){
                res.json({message: "Error!!!!", error: err});
            }
            else{
            product.name = req.body.name, product.quantity=req.body.quantity, product.price=req.body.price;
            product.save(function(err){
                if(err){
                    res.json({message: "Error!", error: err});
                }
                else{
                    res.json({message: "Success!", product: product})
                }
            })
            }
        })
    },
    }