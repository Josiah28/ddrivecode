// Lets import our controller. views.py in Django, *_controller.rb in Ruby
// This demo controller exports functions for us to use.
var our_controller = require('../controllers/demo_controller.js');

module.exports = function (app) {
    app.get('/allproducts', our_controller.products);
    app.get('/oneproduct/:id', our_controller.details);
    app.post('/new', our_controller.addProduct);
    app.delete('/delete/:id', our_controller.deleteProduct);
     app.put('/makeedit/:id', our_controller.editProduct);
}


