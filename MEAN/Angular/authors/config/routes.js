// Lets import our controller. views.py in Django, *_controller.rb in Ruby
// This demo controller exports functions for us to use.
var our_controller = require('../controllers/demo_controller.js');

module.exports = function (app) {
    app.get('/tasks', our_controller.index);
    app.get('/tasks/:id', our_controller.details);
    app.post('/tasks', our_controller.addTask);
    app.put('/tasks/:id', our_controller.editTask);
    app.delete('/tasks/:id', our_controller.deleteTask);
}