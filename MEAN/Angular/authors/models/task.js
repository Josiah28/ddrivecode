var mongoose = require('mongoose');

var TaskSchema = new mongoose.Schema({
    author: {type: String, default: ""}
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Task", TaskSchema);