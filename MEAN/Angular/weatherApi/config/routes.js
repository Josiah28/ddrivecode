var our_controller = require('../controllers/demo_controller.js');

module.exports = function (app) {
    app.get('/', our_controller.index);
}