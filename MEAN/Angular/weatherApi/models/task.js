var mongoose = require('mongoose');

var TaskSchema = new mongoose.Schema({
    humidity: {type: String, default: ""},
    tempAvg:{type: String, default: ""},
    tempHigh: {type: String, default: ""},
    tempLow: {type: String, default: ""},
    status: {type: String, default: ""}
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Task", TaskSchema);