var myString: any;
// I can assign myString like this:
myString = "Bee stinger";
// changed type to any
myString = 9;

function sayHello(name: string){
    return `Hello, ${name}!`;
}
// This is working great:
console.log(sayHello("Kermit"));
//changed 9 to a string"
console.log(sayHello('9!'));


function fullName(firstName: string, lastName: string, middleName?: string){
let fullName = `${firstName} ${middleName} ${lastName}`;
return fullName;
}
// This works:
console.log(fullName("Mary", "Moore", "Tyler"));
// put a question mark on middle name to make it optional
console.log(fullName("Jimbo", "Jones"));


interface Student {
    firstName: string;
    lastName: string;
    belts: number;
}
function graduate(ninja: Student){
    return `Congratulations, ${ninja.firstName} ${ninja.lastName}, you earned ${ninja.belts} belts!`;
}
const christine = {
    firstName: "Christine",
    lastName: "Yang",
    belts: 2
}
 const jay = {
    firstName: "Jay",
    lastName: "Patel",
    belts: 4 //changed typo "belt" to "belts"
}
// This seems to work fine:
console.log(graduate(christine));
// This one has problems:
console.log(graduate(jay));


class Ninja {
    fullName: string;
    constructor(
       public firstName: string,
       public lastName: string){
          this.fullName = `${firstName} ${lastName}`;
       }
    debug(){
       console.log("Console.log() is my friend.")
    }
}
 
// This is not making an instance of Ninja, for some reason:
const shane = new Ninja("Josiah", "Jamison");
console.log(shane.debug());
// Since I'm having trouble making an instance of Ninja, I decided to do this:
const turing = {
    fullName: "Alan Turing",
    firstName: "Alan",
    lastName: "Turing",
    debug(){} //added this line
}
// Now I'll make a study function, which is a lot like our graduate function from above:
function study(programmer: Ninja){
    return `Ready to whiteboard an algorithm, ${programmer.fullName}?`
}
// Now this has problems:
console.log(study(turing));