var express = require('express'),
    app = express(),
    path = require('path'),
    bodyParser = require('body-parser'),
    mongoose = require('./config/mongoose.js'),
    port = 1400;

app.use(express.static( __dirname + '/public/dist/public' ));

app.set(path.join('views', __dirname, 'views'));

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

require('./config/routes.js')(app);

app.listen(1400, function () {
    console.log(`listening on port ${port}`);
})