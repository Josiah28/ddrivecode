import { Component, OnInit } from '@angular/core';
import { HttpService } from './http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  newTask: any;
  tasks: any;
  editTask: any;

  constructor(private _httpService: HttpService){}

  ngOnInit(){
    this.getAllTasks();
    this.newTask = {title: "", description: "" };
  }

  createTask() {
    let observible = this._httpService.addTask(this.newTask);
    observible.subscribe(newData => {
      console.log("New Data", newData);
    })
    this.newTask = { title: "", description: "" }
  }

  getAllTasks(){
    let observable = this._httpService.getTasks();
    observable.subscribe(data => {
      console.log("Got our tasks!", data)
      this.tasks = data;
    });
  }

  updateTasks(){
    let observible = this._httpService.updateTask(this.editTask);
    observible.subscribe(updated => {
      console.log("Updated Task!", updated);
      this.editTask = updated[0];
    })
  }

  editTasks(task){
    this.editTask = task;
  }
}