import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private _http: HttpClient) {
   }

  addTask(newTask){
  return this._http.post('/tasks', newTask)
  }

getTasks(){
  return this._http.get('/tasks');
  }

updateTask(id){
  return this._http.put('/tasks/' + id, id)
  }

}