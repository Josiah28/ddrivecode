var our_controller = require('../controllers/controller.js');

module.exports = function (app) {
    app.get('/', our_controller.index); 
    app.get('/tasks', our_controller.tasks);
    app.post('/tasks', our_controller.create);
    app.get('/tasks/:id', our_controller.showtask);
    app.put('/tasks/:id', our_controller.update);
    app.delete('/tasks/:id', our_controller.delete);
}