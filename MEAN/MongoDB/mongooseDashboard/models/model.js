const mongoose = require('mongoose');

const dashboardSchema = new mongoose.Schema({
    animal: {
        type: String,
        default: '',
        required: true,
        minlength: 2
      },
      age: {
        type: String,
        default: '',
        required: true,
        minlength: 1
      },
      name: {
        type: String,
        default: '',
        required: true,
        minlength: 2
      }
}, {timestamps: true });

const dashboard = mongoose.model('dashboard', dashboardSchema);
module.exports = dashboard;