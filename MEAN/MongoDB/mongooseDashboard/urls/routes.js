var ourController = require('../controllers/controller');

module.exports = function(app) {
  app.get('/', ourController.indexFunction);
  app.get('/animals/new', ourController.createAnimal);
  app.get('/animal/:id', ourController.oneAnimal);
  app.post('/animals', ourController.createFunctionality);
  app.get('/animals/edit/:id', ourController.editAnimal);
  app.post('/animals/:id', ourController.editFunctionality);
  app.post('/animals/destroy/:id', ourController.deleteAnimal);
}