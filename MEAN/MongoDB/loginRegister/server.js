var express    = require('express'),
    app        = express(),
    path       = require('path'),
    port       = 1400;

const flash = require('express-flash');

const session = require('express-session');

const mongoose = require('mongoose');

app.use(session({
    secret: 'k+j=<3',
    resave: false,
    saveUninitialized: true
}))

app.use(express.static(path.join(__dirname, 'static')));

app.set(path.join('views', __dirname, 'views'));

app.set('view engine', 'ejs');

app.use(express.static(__dirname + "/static"));

app.use(express.urlencoded({extended: true}));

app.use(flash());

require('./config/routes.js')(app);

app.listen(1400, function() {
    console.log(`listening on port ${port}`);
})