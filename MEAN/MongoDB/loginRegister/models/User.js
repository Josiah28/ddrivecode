var mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    first_name: {
        type: String,
        default:"",
        required: [true, "First name must be atleast 2 Characters long."],
        minlength: 2,
    },
    last_name: {
        type: String,
        default:"",
        required: [true, "Last name must be atleast 2 Characters long."],
        minlength: 2,
    },
    email: {
        type: String,
        default:"",
        required: [true, "Must enter an E-mail"],
    },
    password: {
        type: String,
        default:"",
        required: true
    },
}, {timestamps:true});
// create an object to that contains methods for mongoose to interface with MongoDB
const User = mongoose.model('User', UserSchema);
module.exports = User;
