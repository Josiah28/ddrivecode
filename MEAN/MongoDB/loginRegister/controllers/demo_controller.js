var User = require('../models/User.js')
var bcrypt = require('bcrypt')
const mongoose = require('../config/mongoose');
module.exports = {
  
  index: function(req, res){
    let users = User.find({}, function (err, users){
      res.render('index');
    })
  },

  register: function(req, res){
    console.log("start")
    let user = new User();
    user.first_name = req.body.fname;
    user.last_name = req.body.lname;
    console.log(user)
    User.findOne({email: req.body.email})
    .then(found_user => {
      console.log("before check",user)
      if(!found_user){
        console.log("good")
        user.email = req.body.email
        if(req.body.password == req.body.confirm_password){
          bcrypt.hash(req.body.password, 10)
          .then(hashed_password => {
            user.password = hashed_password;
            console.log("password", user)
            user.save()
              .then(newUserData => {
                req.session.user_id = user._id
                console.log("session now is",req.session.user_id)
                res.redirect('/user/'+ user._id)
              })
              .catch(err => {
                console.log("We have an error!", err);
                // adjust the code below as needed to create a flash message with the tag and content you would like
                for (var key in err.errors) {
                    req.flash('registration', err.errors[key].message);
                }
                res.redirect("/");
              });
          })
          .catch(error => {

          });
      }
      else{
        console.log("bad")
        req.flash('registration', "Password does not match requirements.")
      } 
  }
  else{
    console.log("found user with email")
    res.redirect("/");
  }   
  })
},

  display:function(req,res){
    console.log("display")
    User.findOne({_id:req.session.user_id})
      .then(user=>{
        console.log("then",user)
        res.render("display", {user: user})
      })
      .catch(error=>{

      })
  },

  login: function(req, res){
    let user = User.find({email: req.body.email}, function (err, user){
      if(user.length > 0){
        bcrypt.compare(req.body.password, user[0].password)
        .then(result => {
          if(result){
            req.session.user_id = user[0]._id;
          }
          else{

          }
        })
        .catch(error => {
        })
      }
      else{
        res.flash("registration", "Wrong user information")
      }
      
    })
      .then(newUserData => {
        res.redirect('/user/'+ user[0]._id)
      })
      .catch(err => {
        console.log("We have an error!", err);
        // adjust the code below as needed to create a flash message with the tag and content you would like
        for (var key in err.errors) {
            req.flash('/', err.errors[key].message);
        }
        res.redirect("/");
      });
      res.redirect("/user/" + req.session.user_id)
    },
}