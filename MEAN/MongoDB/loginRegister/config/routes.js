var our_controller = require('../controllers/demo_controller.js');

module.exports = function(app) {
  app.get('/', our_controller.index);
  app.post('/register', our_controller.register);
  app.get('/user/:id', our_controller.display);
  app.post('/login', our_controller.login);
}