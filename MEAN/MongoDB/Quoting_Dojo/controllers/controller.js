var Quote = require("../models/quotes");

module.exports = {
    indexFunction: function(req, res){
        res.render('index');
    },
    makeQuote: function(req, res){
        const quote = new Quote();
        quote.creator = req.body.name;
        quote.quote = req.body.quote;
        quote.save()
          .then(newCreatorData => {
            console.log(newCreatorData);
            res.redirect('/quotes')
          })
          .catch(err => {
            console.log(err)
            res.redirect('/');
          })
    },
    quotesFunction: function(req, res){
        quotes = Quote.find({}, function(err, quotes) {
            if (err) {
              console.log("Quote not valid.");
            }
            else {
              res.render('quotes', {allQuotes: quotes});
            }
          });
    }
}