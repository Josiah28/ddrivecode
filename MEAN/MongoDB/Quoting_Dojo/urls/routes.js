var ourController = require('../controllers/controller');

module.exports = function(app) {
  app.get('/', ourController.indexFunction);
  app.post('/quotes', ourController.makeQuote);
  app.get('/quotes', ourController.quotesFunction);
}