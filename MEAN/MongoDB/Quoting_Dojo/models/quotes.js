var mongoose = require('mongoose');

var QuoteSchema = new mongoose.Schema({
  quote: {
    type: String,
    default: '',
  },
  creator: {
    type: String,
    default: '',
  }
}, {timestamps: true });

var Quote = mongoose.model('Quote', QuoteSchema);
module.exports = Quote;