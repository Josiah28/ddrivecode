var express    = require('express'),
    app        = express(),
    path       = require('path'),
    port       = 1400;

mongoose = require('./urls/mongoose.js'),

app.use(express.static(path.join(__dirname, 'static')));

app.set(path.join('views', __dirname, 'views'));

app.set('view engine', 'ejs');

app.use(express.urlencoded({extended: true}));

require('./urls/routes.js')(app);

app.listen(1400, function() {
    console.log(`listening on port ${port}`);
})