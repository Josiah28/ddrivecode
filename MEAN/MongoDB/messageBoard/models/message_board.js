var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
    name: { type: String, required: [true, 'Name is required'], minlength: [2, 'Name must be at least two characters'] },
    comment: { type: String, required: [true, 'Please input a comment'], maxlength: [255, 'Comment is too long!'] }
  }, { timestamps: true });
  
var MessageSchema = new mongoose.Schema({
    name: { type: String, required: [true, 'Name is required'], minlength: [2, 'Name must be at least two characters'] },
    messages: { type: String, required: [true, 'Please input a message'], maxlength: [255, ' Message is too long!'] },
    comments: [CommentSchema]
  }, { timestamps: true });


var Message = mongoose.model('Message', MessageSchema);
var Comment =  mongoose.model('Comment', CommentSchema);
//module.exports = Message;
//module.exports = Comment;

module.exports = {
  msg: Message,
  cmt: Comment
}