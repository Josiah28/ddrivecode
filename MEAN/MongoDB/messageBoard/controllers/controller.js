var MessageModel = require('../models/message_board.js').msg;
var CommentModel = require('../models/message_board.js').cmt;

module.exports = {
    index: function(req, res) {
        MessageModel.find()
        // console.log("messages: ", messages.id)
        .then(all_data => {console.log(all_data), res.render('index', {allMessages: all_data})})
        .catch(err => {
            console.log("We have an error!", err);
            // adjust the code below as needed to create a flash message with the tag and content you would like
            for (var key in err.errors) {
                req.flash('Error', err.errors[key].message);
            };
    })
    },
    add_message: function(req, res) {
        let new_message = new MessageModel();
        console.log("Message before assignment is :", new_message);
        new_message.name = req.body.message_name;
        new_message.messages = req.body.message_message;
        console.log(new_message.name)
        console.log(new_message.messages)
        console.log("Message before being saved: ", new_message);
        new_message.save()
        .then(newMessageData =>{ console.log(newMessageData); res.redirect('/')})
        .catch(err => {
            console.log("We have an error!", err);
            // adjust the code below as needed to create a flash message with the tag and content you would like
            for (var key in err.errors) {
                req.flash('Error', err.errors[key].message);
            }
            res.redirect('/');
        });
    },
    add_comment: function(req, res) {
        let {mid} = req.params;
        let new_comment = new CommentModel();
        console.log("req.params.id: ", mid)
        console.log("Comment before assignment is: ", new_comment);
        // new.comment.
        new_comment.name = req.body.name;
        new_comment.comment = req.body.comment;
        console.log("Comment before being saved: ", new_comment);
        new_comment.save()
        .then(newCommentData =>{  
            MessageModel.updateOne({_id: mid}, {$push: {comments: newCommentData}})
            .then(data => res.redirect('/'))
            .catch(err => {
                console.log("We have an error!", err);
                // adjust the code below as needed to create a flash message with the tag and content you would like
                for (var key in err.errors) {
                    req.flash('Error', err.errors[key].message);
                }
                res.redirect('/');
            });
        })
        .catch(err => {
            console.log("We have an error!", err);
            // adjust the code below as needed to create a flash message with the tag and content you would like
            for (var key in err.errors) {
                req.flash('Error', err.errors[key].message);
            }
            res.redirect('/');
        });
    }


       
}