var express    = require('express'),
    app        = express(),
    path       = require('path'),
    bodyParser = require('body-parser'),
    mongoose   = require('./config/mongoose.js'),
    port       = 1400;
    flash      = require('express-flash');

const session = require('express-session');
    app.use(session({
    secret: 'k+j=<3',
    resave: false,
    saveUninitialized: true,
    }))
app.use(express.static(path.join(__dirname, 'static')));

app.set(path.join('views', __dirname, 'views'));

app.set('view engine', 'ejs');
app.use(express.urlencoded({extended: true}));

app.use(flash());

require('./config/routes.js')(app);

app.listen(1400, function() {
    console.log(`listening on port ${port}`);
})