var Duck = require('../models/duck.js');


module.exports = {

  index: function(req, res){
    Duck.find({}, function(err, ducks) {
      if (err) {
        res.redirect('/')
      }
      else {
        res.render('index', {all_ducks: ducks});
      }
    })
  },

  add_duck: function(req, res){
    console.log("add_duck", req.body)
    let duck = new Duck()
    console.log("Duck before assignment is", duck)
    duck.name = req.body.name;
    duck.age = req.body.age;
    duck.favorite_food = req.body.food;
    duck.color = req.body.color;
    console.log("duck before save is", duck)
    duck.save()
    .then(newDuck => {
        req.session['duck_id'] = newDuck._id
        res.redirect('/')
    })
    .catch(err => {
        console.log("We have an error!", err)
        for (var key in err.errors) {
            req.flash('error', err.errors[key].message)
        }
            res.redirect('/')
    })
  },

  one_duck: function(req, res){
    console.log("one_duck", req.body)
    Duck.findOne({_id: req.params.id}, function(err, ducks) {
        if (err){
          for(var key in err.errors){
              req.flash("error", err.errors[key].message);
          }
          res.redirect('/')
        }
        else{
          res.render('detail', {duck: ducks })
        }

    })
  },

  new_duck: function(req, res) {
    res.render('new_duck')
  },


  edit_duck: function(req, res) {
    Duck.find({_id: req.params.id}, function(err, duck) {
      if (err) {
        res.redirect('/')
      }
      else {
        res.render('edit_duck', {duck: duck[0]})
      }
    })
  },

  update_duck: function(req, res) {
    Duck.findOne({_id: req.params.id}, function(err, ducks) {
      console.log("update_duck",ducks)
      ducks.name = req.body.name
      ducks.age = req.body.age
      ducks.favorite_food = req.body.food
      ducks.color = req.body.color
      ducks.save()
      .then(updatedDuck => {
        res.redirect('/')
      })
      .catch(err => {
        for(var key in err.errors){
          req.flash("error", err.errors[key].message);
        }
          res.redirect('/')

      })
    })
  },
  
  destroy_duck: function(req, res) {
    Duck.remove({_id: req.params.id})
      .then(deletedDuck => {
        res.redirect('/')
      })
      .catch(err => res.json(err));
  }
}