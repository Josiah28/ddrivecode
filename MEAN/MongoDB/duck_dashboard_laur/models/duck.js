var mongoose = require('mongoose');

var DuckSchema = new mongoose.Schema({
  name: {
    type: String,
    default: '',
    required: [true, "Name must be at least two characters."],
    minlength: 2
  },
  age: {
  	type: String,
  	default: '',
  	required: [true, "Please enter a valid age."],
  },
  favorite_food: {
    type: String,
    default: '',
    required: [true, "Please enter a valid favorite food."],
    minlength: 1
  },
  color: {
    type: String,
    default: '',
    required: [true, "Please enter a valid color"],
    minlength: 2
  },
}, {timestamps: true });

var Duck = mongoose.model('Duck', DuckSchema);
module.exports = Duck;