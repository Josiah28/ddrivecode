// Lets import our controller. views.py in Django, *_controller.rb in Ruby
// This demo controller exports functions for us to use.
var our_controller = require('../controllers/controller.js');

module.exports = function(app) {

  app.get('/', our_controller.index);
  app.get('/duck/new', our_controller.new_duck);
  app.post('/duck', our_controller.add_duck);
  app.get('/duck/:id', our_controller.one_duck);
  app.get('/duck/edit/:id', our_controller.edit_duck);
  app.post('/duck/:id', our_controller.update_duck);
  app.post('/duck/destroy/:id', our_controller.destroy_duck);

}