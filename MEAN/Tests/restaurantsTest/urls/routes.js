var myController = require('../controllers/controller.js');

module.exports = function (app) {
    app.get('/restaurants', myController.restaurants); //all restaurants
    app.post('/new', myController.createRestaurant); //create restaurantt
    app.get('/restaurants/:id', myController.reviews); //restaurants reviews
    app.delete('/delete/:id', myController.delete); //delete restaurant
    app.post('/restaurants/:id', myController.writeReview); //write review
    app.put('/restaurants/:id', myController.editRestaurant); //edit restaurant
}