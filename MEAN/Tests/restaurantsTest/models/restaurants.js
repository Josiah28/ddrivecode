var mongoose = require('mongoose');
const Schema = mongoose.Schema

var RestaurantSchema = new mongoose.Schema({
    title: {type: String, default: "", required: [true, "Cannot be empty"], minlength: [3, "Restaurant must be at least 3 characters"]},
    cuisine: {type: String, default: "", required: [true, "Cannot be empty"], minlength: [3, "Cuisine must be at least 3 characters"]},
    review: [{type: Schema.Types.Mixed, ref: "Review"}]
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Restaurant", RestaurantSchema);

var ReviewSchema = new mongoose.Schema({
    name: {type: String, default: "", required: [true, "Cannot be empty"], minlength: [3, "Name must be at least 3 characters"]},
    stars: {type: Number, default: 0, required: [true, "Must give at least one star"]},
    reviews: {type: String, default: "", required: [true, "Cannot be empty"], minlength: [3, "Review must be at least 3 characters"]}
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Review", ReviewSchema);