import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  restaurants: any;
  restaurant: any;
  newRestaurant: any;
  typo: any;
  typo2: any;
  typo3: any;
  typo4: any;
  typo5: any;
  error: any;
  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => { 
    this.restaurant = {title: "", cuisine: " ", name: "", stars: "", reviews: ""}
  });
  } 
  goHome() {
    this._router.navigate(['/home']);
  }

 createRestaurant() {
    let observable = this._httpService.createRestaurant(this.restaurant);
    observable.subscribe((data: any) =>{
      console.log("Created Restaurant!", data);
      if (data.error){console.log(data.error)
          this.typo = data.error.errors.name.message
          this.typo2 = data.error.errors.stars.message
          this.typo3 = data.error.errors.reviews.message
          this.typo4 = data.error.errors.title.message
          this.typo5 = data.error.errors.cuisine.message
        }
        else{
          this.restaurant = {title: "", cuisine: " ", name: "", stars: "", reviews: ""}
          this.goHome();
        }
      //this.restaurant = data;
    })
  }
}