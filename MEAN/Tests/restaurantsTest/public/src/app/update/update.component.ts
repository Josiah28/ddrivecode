import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  restaurants:any;
  restaurant: any;
  restaurantid: any;
  typo: any;
  typo2: any;
  error: any;

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this.restaurant = {title:"", cuisine: ""}
    this._route.params.subscribe((params: Params) => {
      console.log(params["id"])
      let observable = this._httpService.getOne(params['id'])
      observable.subscribe(data =>{
        console.log(data);
        console.log(this.restaurant.title);
        this.restaurants=data
      })
    })
    this.getParams();
  }

  getTasksFromService(){
    let observable = this._httpService.getRestaurants();
    observable.subscribe(data => { 
      console.log("~Loading All Restaurants~", data);
      this.restaurants = data["restaurant"]
    });
  }

  onEdit(){
    console.log(this.restaurant)
    let observable = this._httpService.editRestaurant(this.restaurantid, this.restaurant)
    observable.subscribe((data: any) => {
      console.log("~Edit~");
      console.log(data);
      if(data.error){
        this.typo = data.error.errors.title.message
        this.typo2 = data.error.errors.reviews.message
      }
    })
    this.goHome();
  }

  getParams(){
    this._route.params.subscribe((params: Params) => {
      console.log("Got ID", params.id)
      this.restaurantid = params.id
    })
  }

  goHome() {
    this._router.navigate(['/home']);
  }

}
