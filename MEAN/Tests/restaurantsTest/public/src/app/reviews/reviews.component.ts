import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
restaurants:any;
id:any;
review:any;
reviews:any;

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
      this.id = params.id
      console.log(params['id'])
      this.getRestaurantFromService(params['id'])
  });
  }

  goHome() {
    this._router.navigate(['/showreviews/:id']);
  }

  getRestaurantFromService(id){
    let observable = this._httpService.getOne(id);
    observable.subscribe(data => { 
      console.log("~Loading All Tasks~", data)
      this.restaurants = data["restaurant"]
      console.log("LETS SEE")
    });
  }
  onDelete(){
    console.log(this.id)
    let observable = this._httpService.deleteRestaurant(this.id);
    observable.subscribe((data: any) => {
      if(data.errors){
        console.log(data.errors)
      }
      else{
        console.log("~Delete~", data);
      }
     })
     this.goHome();
  }
}