import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }


  createRestaurant(newRestaurant){
    console.log("Tetsing")
    return this._http.post('/new', newRestaurant);
  }

  getRestaurants(){
    return this._http.get('/restaurants');
  }

  writeReview(id, newreview){
    return this._http.post(`/restaurants/${id}`, newreview)
  }

  getOne(id){
    console.log("In get one http")
      return this._http.get(`/restaurants/${id}`);
  }

  deleteRestaurant(id){
    return this._http.delete(`/delete/${id}`);
  }

  editRestaurant(id, restaurant){
    console.log("Http")
    return this._http.put(`/restaurants/${id}`,restaurant);
  }
}