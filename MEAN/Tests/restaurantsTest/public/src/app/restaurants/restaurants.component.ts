import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit {
  restaurants: any;
  restaurant: "";

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
    });
    this.getRestaurantsFromService()
  }

  getRestaurantsFromService(){
    console.log("get restaurant from service")
    let observable = this._httpService.getRestaurants();
    observable.subscribe(data => {
      console.log("Loading all restaurants", data)
      this.restaurants = data
      console.log("end of get restaurants from service", data)
    })
  }

  goHome() {
    this._router.navigate(['/restaurants']);
  }

}
