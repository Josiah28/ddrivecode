var mongoose = require("mongoose");
var Restaurant = mongoose.model("Restaurant");
var Review = mongoose.model("Review");
module.exports = {
    restaurants: function(req, res){
        Restaurant.find({}, function(err, restaurants){
            if(err){
                console.log("Error finding restaurant")
                res.json({message: "Error", error: err});
            }
            else{
                res.json(restaurants);
                console.log("Got restaurant")
            }
        })
    },

    createRestaurant: function(req, res){
        Review.create({name: req.body.name, stars: req.body.stars, reviews: req.body.reviews}, function(err, review){
            if(err){
                console.log("Error");
                res.json({message: "Error", error: err})
            }
            else if(err){
                Restaurant.create({title: req.body.title, cuisine: req.body.cuisine}, function(err, restaurant){
                    if(err){
                        console.log("Error")
                        res.json({message: "Error", error: err})
                    }
                    else{
                        console.log("Here is the review", review)
                        restaurant.review.push(review)
                        restaurant.save()
                        console.log("Restaurant", restaurant)
                        res.json({message: "Success!", added: true});
                    }
                })
            }
        })
    },

    reviews: function(req, res){
        console.log("in details")
        let id = req.params.id;
        Restaurant.find({_id: id},function(err, restaurant){
            if(err){
                console.log("here in error")
                res.json({message: "error", error: err});
            }
            else{
                console.log(restaurant)
                res.json({message: "success", restaurant: restaurant[0]});
            }
        });
    },

    delete: function(req, res){
        console.log(req.params.id);
        Restaurant.deleteOne({_id:req.params.id})
        .then(data => {
            res.json(data, "deleted data")
        })
        .catch(err => {
            res.json(err)
        })
    },

    writeReview: function(req, res){
        console.log('start add review')
        Review.create({name: req.body.name, stars: req.body.stars, reviews: req.body.reviews},function(err,review){
            if(err){
                res.json({message: "Error with review"})
            }
            else{
                Restaurant.findOne({_id: req.params.id},function(err, restaurant){ //find always bring an array of objs even if just one
                    if(err){
                        console.log("here in error")
                        res.json({message: "error", error: err});
                    }
                    else{
                        console.log(restaurant)
                        restaurant.review.push(review)
                        restaurant.save()
                        console.log(restaurant)

                        res.json(restaurant);
                    }
                });
 
                }
            })
    },
    editRestaurant: function(req, res){
        console.log(req.body, "params")
        let id = req.params.id;
        console.log(id, "Controller")
        Restaurant.findOneAndUpdate({_id: id}, req.body, {new: true})
            .then(data => {
                res.json(data)
            })
            .catch(err => {
                res.json(err)
                res.json({message: "error", error: err});
            })
    }
}