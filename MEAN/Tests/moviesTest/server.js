var express = require('express'),
    app = express(),
    path = require('path'),
    bodyParser = require('body-parser'),
    mongoose = require('./urls/mongoose.js'),
    port = 1400;

app.use(express.static( __dirname + '/public/dist/public' ));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

require('./urls/routes.js')(app);

app.all("*", (req,res,next) => {
    res.sendFile(path.resolve("./public/dist/public/index.html"))
  });

app.listen(1400, function () {
    console.log(`listening on port ${port}`);
})