var myController = require('../controllers/controller.js');

module.exports = function (app) {
    app.get('/movies', myController.movies);
    app.post('/new', myController.createMovie);
    app.get('/reviews/:id', myController.reviews); //read review
    app.delete('/delete/:id', myController.delete);
    app.post('/reviews/:id', myController.writeReview);
    app.put('/movies/:id', myController.editMovie);
}