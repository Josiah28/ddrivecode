import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }


  createMovie(newMovie){
    console.log("Tetsing")
    return this._http.post('/new', newMovie);
  }

  getMovies(){
    return this._http.get('/movies');
  }

  writeReview(id, newreview){
    return this._http.post(`/reviews/${id}`, newreview)
  }

  getOne(id){
    console.log("In get one http")
      return this._http.get(`/reviews/${id}`);
  }

  deleteMovie(id){
    return this._http.delete(`/delete/${id}`);
  }

  editMovie(id,movie){
    console.log("Http")
    return this._http.put(`/movies/${id}`,movie);
  }
}