import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  movies: any;
  movie: any;
  newMovie: any;
  rest = {name: "", stars: "", review: ""}
  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => { 
    this.movie = {title: "", name: "", stars: "", reviews: ""} // this object is the form
  });
  } 
  goHome() {
    this._router.navigate(['/home']);
  }

 createMovie() {
    let observable = this._httpService.createMovie(this.movie);
    observable.subscribe((data: any) =>{
      console.log("Created Movie!", data);
      if (data.error){console.log(data.error)
        }
      this.movie = data;
    })
    this.goHome();
  }
}