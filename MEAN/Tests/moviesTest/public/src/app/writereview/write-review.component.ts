import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-write-review',
  templateUrl: './write-review.component.html',
  styleUrls: ['./write-review.component.css']
})
export class WriteReviewComponent implements OnInit {
  mistake: any;
  review: any;
  reviewid: any;

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this.getParams();
    this.review = {name: "", stars: "", reviews: ""}
  }

  createReview(){
    console.log('Start function')
    let obs = this._httpService.writeReview(this.reviewid, this.review);
    obs.subscribe((data: any) => {
      console.log('creating review', data);
      if (data.errors) { 
        console.log('errors', data.errors)
      } else {
        console.log('It worked')
      }
    })
    this.goHome();
  }

  getParams(){
    this._route.params.subscribe((params: Params) => {
      console.log("Got ID", params.id)
      this.reviewid = params.id
    })
  }

  goHome() {
    this._router.navigate(['/home']);
  }
}