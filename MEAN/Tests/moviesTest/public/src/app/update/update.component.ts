import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  movies:any;
  movie: any;
  movieid: any;

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this.movie = {title:""}
    this._route.params.subscribe((params: Params) => {
      console.log(params["id"])
      let observable = this._httpService.getOne(params['id'])
      observable.subscribe(data =>{
        console.log(data)
        this.movies=data
      })
    })
    this.getParams();
  }

  getTasksFromService(){
    let observable = this._httpService.getMovies();
    observable.subscribe(data => { 
      console.log("~Loading All Movies~", data)
      this.movies = data["movie"]

    });
  }

  onEdit(){
    console.log(this.movie)
    let observable = this._httpService.editMovie(this.movieid, this.movie)
    observable.subscribe(data => {
      console.log("~Edit~");
      console.log(data)
    })
    this.goHome();
  }

  getParams(){
    this._route.params.subscribe((params: Params) => {
      console.log("Got ID", params.id)
      this.movieid = params.id
    })
  }

  goHome() {
    this._router.navigate(['/home']);
  }

}
