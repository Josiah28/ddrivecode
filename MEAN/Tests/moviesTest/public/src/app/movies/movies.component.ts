import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  movies: any;
  movie: "";

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
    });
    this.getMoviesFromService()
  }

  getMoviesFromService(){
    console.log("get movie from service")
    let observable = this._httpService.getMovies();
    observable.subscribe(data => {
      console.log("Loading all movies", data)
      this.movies = data
      console.log("end of get movies from service", data)
    })
  }

  goHome() {
    this._router.navigate(['/movies']);
  }
}