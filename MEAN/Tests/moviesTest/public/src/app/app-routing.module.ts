import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { CreateComponent } from './create/create.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { WriteReviewComponent } from './writereview/write-review.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  { path: 'home',component: MoviesComponent },
  { path: 'create',component: CreateComponent },
  { path: 'showreviews/:id',component: ReviewsComponent },
  { path: 'writereview/:id',component: WriteReviewComponent },
  { path: 'update/:id',component: UpdateComponent },
  { path: '', pathMatch: 'full', redirectTo: '/home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }