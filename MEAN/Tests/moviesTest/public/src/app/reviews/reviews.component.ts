import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
movies:any;
id:any;
review:any;
reviews:any;

  constructor(private _route: ActivatedRoute,
    private _router: Router, private _httpService: HttpService) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
      this.id = params.id
      console.log(params['id'])
      this.getMovieFromService(params['id'])
  });
  }

  goHome() {
    this._router.navigate(['/showreviews/:id']);
  }

  getMovieFromService(id){
    let observable = this._httpService.getOne(id);
    observable.subscribe(data => { 
      console.log("~Loading All Tasks~", data)
      this.movies = data["movie"]
      console.log("LETS SEE")
    });
  }
  onDelete(){
    console.log(this.id)
    let observable = this._httpService.deleteMovie(this.id);
    observable.subscribe((data: any) => {
      if(data.errors){
        console.log(data.errors)
      }
      else{
        console.log("~Delete~", data);
      }
     })
  }
}