var mongoose = require("mongoose");
var Movie = mongoose.model("Movie");
var Review = mongoose.model("Review");
module.exports = {
    movies: function(req, res){
        Movie.find({}, function(err, movies){
            if(err){
                console.log("Error finding movie")
                res.json({message: "Error", error: err});
            }
            else{
                res.json(movies);
                console.log("Got movie")
            }
        })
    },

    createMovie: function(req, res){
        Review.create({name: req.body.name, stars: req.body.stars, reviews: req.body.reviews}, function(err, review){
            if(err){
                console.log("Error w review");
                res.json({message: "Error with review", error: err})
            }
            else{
                Movie.create({title: req.body.title}, function(err, movie){
                    if(err){
                        console.log("Error2")
                        res.json({message: "Error with movie", error: err})
                    }
                    else{
                        console.log("Here is the review", review)
                        movie.review.push(review)
                        movie.save()
                        console.log("Movie", movie)
                        res.json({message: "Success!", added: true});
                    }
                })
            }
        })
    },

    reviews: function(req, res){
        console.log("in details")
        let id = req.params.id;
        Movie.find({_id: id},function(err, movie){
            if(err){
                console.log("here in error")
                res.json({message: "error", error: err});
            }
            else{
                console.log(movie)
                res.json({message: "success", movie: movie[0]});
            }
        });
    },

    delete: function(req, res){
        console.log(req.params.id);
        Movie.deleteOne({_id:req.params.id})
        .then(data => {
            res.json(data, "deleted data")
        })
        .catch(err => {
            res.json(err)
        })
    },

    writeReview: function(req, res){
        console.log('start add review')
        Review.create({name: req.body.name, stars: req.body.stars, reviews: req.body.reviews},function(err,review){
            if(err){
                res.json({message: "Error with review"})
            }
            else{
                Movie.findOne({_id: req.params.id},function(err, movie){ //find always bring an array of objs even if just one
                    if(err){
                        console.log("here in error")
                        res.json({message: "error", error: err});
                    }
                    else{
                        console.log(movie)
                        movie.review.push(review)
                        movie.save()
                        console.log(movie)

                        res.json(movie);
                    }
                });
 
                }
            })
    },
    editMovie: function(req, res){
        console.log(req.body, "params")
        let id = req.params.id;
        console.log(id, "Controller")
        Movie.findOneAndUpdate({_id: id}, req.body, {new: true})
            .then(data => {
                res.json(data)
            })
            .catch(err => {
                res.json(err)
            })
    }
}