var mongoose = require('mongoose');
const Schema = mongoose.Schema

var MovieSchema = new mongoose.Schema({
    title: {type: String, default: "", required: [true, "Cannot be empty"]},
    review: [{type: Schema.Types.Mixed, ref: "Review"}]
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Movie", MovieSchema);

var ReviewSchema = new mongoose.Schema({
    name: {type: String, default: "", required: [true, "Cannot be empty"]},
    stars: {type: Number, default: 0, required: [true, "Cannot be empty"]},
    reviews: {type: String, default: "", required: [true, "Cannot be empty"]}
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}});

mongoose.model("Review", ReviewSchema);