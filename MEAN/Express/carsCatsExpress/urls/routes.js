var ourController = require('../controllers/controller');

module.exports = function(app) {
  app.get('/index', ourController.indexFunction);
}

module.exports = function(app) {
  app.get('/cars', ourController.carsFunction);
}

module.exports = function(app) {
  app.get('/cats', ourController.catsFunction);
}

module.exports = function(app) {
  app.get('/form', ourController.formFunction);
}