const express = require("express");
const app = express();
const session = require('express-session');

app.use(express.static(__dirname + "/static"));

app.set('view engine', 'ejs');

app.set('views', __dirname + '/views');

app.use(session({
    secret: 'k+j=<3',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
}))

app.use(express.urlencoded({ extended: true }));

require('./urls/routes.js')(app);

app.listen(1400, () => console.log("listening on port 1400"));