// Export functions for our routes.js file to use. This is where the logic of
// your server will go.
module.exports = {

    home_function: function (req, res) {
        res.render('index');
    },

createSession: function(req, res){
    console.log(req.body)
    console.log("req.session is", req.session)
    req.session.name = req.body.name
    req.session.location = req.body.location
    req.session.language = req.body.language
    req.session.text = req.body.text
    console.log("req.session now is", req.session)
    res.redirect('/results')
},

results: function(req, res){
    console.log("in results, req.session is", req.session)
    res.render('results', {data: req.session})
},
}