var ourController = require('../controllers/controller');

module.exports = function(app) {
  app.get('/cats', ourController.catsFunction);
  app.get('/cat1', ourController.cat1Function);
  app.get('/cat2', ourController.cat2Function);
  app.get('/cat3', ourController.cat3Function);
  app.get('/cats1', ourController.cats1Function);
  app.get('/cats2', ourController.cats2Function);

}