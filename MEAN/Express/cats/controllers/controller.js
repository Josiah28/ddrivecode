module.exports = {
    catsFunction: function(req, res){
        res.render('cats');
    },
    cat1Function: function(req, res){
        var cat1Array = [
            {name: "Stormy", favFood: "Wet Food", age: "10", sleep: "On Blanket"}
        ];
        res.render('details', {cat: cat1Array});
    },
    cat2Function: function(req, res){
        var cat2Array = [
            {name: "Bob", favFood: "Treats", age: "7", sleep: "Under the Bed"}
        ];
        res.render('details', {cat: cat2Array});
    },
    cat3Function: function(req, res){
        var cat3Array = [
            {name: "Fluffy", favFood: "Dry Food", age: "3", sleep: "On the Bed"}
        ];
        res.render('details', {cat: cat3Array});
    },
    cats1Function: function(req, res){
        var cats1Array = [
            {name: "Frisky & Jenna", favFood: "Wet Food", age: "10, 10", sleep: "Under the Covers"}
        ];
        res.render('details', {cat: cats1Array});
    },
    cats2Function: function(req, res){
        var cats2Array = [
            {name: "Andrew", favFood: "Wet Food", age: "14", sleep: "On Carpet"}
        ];
        res.render('details', {cat: cats2Array});
    }
}