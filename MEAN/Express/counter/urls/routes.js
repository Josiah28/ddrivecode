var ourController = require('../controllers/controller');

module.exports = function(app) {
  app.get('/', ourController.counterFunction);
}