const session = require('express-session');
app.use(session({
  secret: 'k+j=<3',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000 }
}))

var express    = require('express'),
    app        = express(),
    path       = require('path'),
    port       = 1400;

app.use(express.static(path.join(__dirname, 'static')));

app.set(path.join('views', __dirname, 'views'));

app.set('view engine', 'ejs');

require('./urls/routes.js')(app);

app.listen(1400, function() {
    console.log(`listening on port ${port}`);
})c