from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
    url(r'^add$', views.add),
    url(r'^login$', views.login),
    url(r'^success$', views.user_home),
    url(r'^destroy$', views.destroy)
]