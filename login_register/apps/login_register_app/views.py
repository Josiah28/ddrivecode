from django.shortcuts import render,redirect
from .models import User
import bcrypt
from django.contrib import messages
from .models import UserManager, User

def index(request):
    return render(request,"login_register_app/index.html")

def add(request):
    if request.method=="POST":
        errors = User.objects.basic_validator(request.POST)

        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashpw=bcrypt.hashpw(request.POST["password"].encode(), bcrypt.gensalt())
            
            new_user=User.objects.create(first_name=request.POST["fname"], last_name=request.POST["lname"], email=request.POST['email'], password=hashpw) 
            
            request.session['id']=new_user.id



            return redirect("/success")

def login(request):

    if request.method=="POST":
        errors={}
        info=User.objects.filter(email=request.POST['email'])
        if len(info)==0:
            return redirect(request,"/success")
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            if bcrypt.checkpw(request.POST["password"].encode(),info[0].password.encode()):
                request.session['id']=info[0].id
                return redirect("/success")
            if not bcrypt.checkpw(request.POST["password"].encode(),info[0].password.encode()):
                errors['password']="wrong password"
                if len(errors)>0:
                    for key, value in errors.items():
                        messages.error(request, value)
                    return redirect('/')

            

        return redirect("/success")

def user_home(request):
    context={
        'user': User.objects.get(id=request.session['id'])
    }
    return render(request,"login_register_app/success.html",context)

def destroy(request):
    request.session.flush()
    return redirect("/")