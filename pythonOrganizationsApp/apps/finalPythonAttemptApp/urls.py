from django.conf.urls import url
from . import views
                    
urlpatterns = [
    url(r'^$', views.index),
     url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^groups$', views.userHome),
    url(r'^addGroup$', views.addGroup),
    url(r'^joinGroup/(?P<id>\d+)$', views.joinGroup),
    url(r'^leaveGroup/(?P<id>\d+)$', views.leaveGroup),
    url(r'^usersGroup/(?P<id>\d+)$', views.view),
    url(r'^delete/(?P<id>\d+)$', views.destroy),
    url(r'^logout$', views.logout)
]